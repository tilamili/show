 package com.tiramisu.show;

 import java.util.ArrayList;

 public class PageBean {
     private String mainId;
     private String orgId;
     private long pageTime;

     public long getPageTime() {
         return pageTime;
     }

     public void setPageTime(long pageTime) {
         this.pageTime = pageTime;
     }

     public String getMainId() {
         return mainId;
     }

     public void setMainId(String mainId) {
         this.mainId = mainId;
     }

     public String getOrgId() {
         return orgId;
     }

     public void setOrgId(String orgId) {
         this.orgId = orgId;
     }

     public ArrayList<AreaBean> areaBeanArrayList=new ArrayList<>();

 }
