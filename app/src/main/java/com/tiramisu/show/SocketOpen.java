package com.tiramisu.show;

import android.text.TextUtils;

import com.tiramisu.show.util.DeviceUtils;
import com.tiramisu.show.util.NetworkUtils;
import com.tiramisu.show.util.SPUtils;
import com.tiramisu.show.util.ScreenUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Map;


public class SocketOpen {
    Socket client = null;

    public String getSocketLog(Map map, String type) {
        String ipAddr = (String) map.get("IP");
        String shortName = (String) map.get("shortName");
        String password = (String) map.get("password");
        int port = Integer.valueOf((String) map.get("port"));
        //与服务端建立连接
        if (TextUtils.isEmpty(ipAddr) || TextUtils.isEmpty(shortName) || TextUtils.isEmpty(password)) {
            return "";
        }
        try {
            if (client != null) {
                client.close();
            }
            client = new Socket();
            SocketAddress socketAddress = new InetSocketAddress(ipAddr, port);
            client.connect(socketAddress, 2000);
        } catch (IOException e) {
            return "error;";
        }
        //建立连接后就可以往服务端写数据了
        if (type.equals("0")) {
            try {
                Writer writer = null;
                writer = new OutputStreamWriter(client.getOutputStream(), "UTF-8");
                writer.write("1;" + shortName + ";" + password + ";");
                writer.write("ends\n");
                writer.flush();
                //写完以后进行读操作
                BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream(), "UTF-8"));
                StringBuffer sb = new StringBuffer();
                String temp;
                int index;
                while ((temp = br.readLine()) != null) {
                    if ((index = temp.indexOf("ends")) != -1) {
                        sb.append(temp.substring(0, index));
                        break;
                    }
                    sb.append(temp);
                }
                System.out.println("我是客户端,服务器说: " + sb);


                client.close();
                return sb.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (type.equals("3")) {
            try {
                Writer writer = null;
                writer = new OutputStreamWriter(client.getOutputStream(), "UTF-8");
                writer.write("3;" + shortName + ";" + password + ";");
                writer.write("ends\n");
                writer.flush();
                //写完以后进行读操作
                BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream(), "UTF-8"));
                StringBuffer sb = new StringBuffer();
                String temp;
                int index;
                while ((temp = br.readLine()) != null) {
                    if ((index = temp.indexOf("ends")) != -1) {
                        sb.append(temp.substring(0, index));
                        break;
                    }
                    sb.append(temp);
                }
                System.out.println("我是客户端,服务器说: " + sb);

                client.close();
                return sb.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (type.startsWith("2;")) {
            try {
                Writer writer = null;
                writer = new OutputStreamWriter(client.getOutputStream(), "UTF-8");
                String temId = type.split(";")[1];
                String ip = NetworkUtils.getInstance(ShowApplication.mApplication).getNetworkIP();
                String hostName = "qf";
                String mac = NetworkUtils.getMacAddress(ShowApplication.mApplication).replace(':', '-');

                String osName = "android"; //操作系统名称
                String osVersion = DeviceUtils.getRELEASEVersion(); //操作系统版本
                int screenWidth = ScreenUtils.getScreenWidth();
                int screenHeight = ScreenUtils.getScreenHeight();
                String resolving = String.valueOf(screenWidth) + "*" + String.valueOf(screenHeight);
                String keyPath = "/sdcard/benzi.bz";
                File keyf = new File(keyPath);
                String date = "";
                if (keyf.exists()) {
                    String datestr = null;
                    try {
                        datestr = goPassUtil.readTxtFile(keyf);
                        date = goPassUtil.getPass(datestr.split("null")[1]).split("_")[0];
                        resolving = resolving + ";" + date;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                writer.write("5;" + temId + ";" + ip + ";" + hostName + ";" + mac + ";" + osName + ";" + osVersion + ";" + resolving);
                writer.write("ends\n");
                writer.flush();
                //写完以后进行读操作
                BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream(), "UTF-8"));
                StringBuffer sb = new StringBuffer();
                String temp;
                int index;
                while ((temp = br.readLine()) != null) {
                    if ((index = temp.indexOf("ends")) != -1) {
                        sb.append(temp.substring(0, index));
                        break;
                    }
                    sb.append(temp);
                }
                client.close();
                return sb.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (type.startsWith("1;")) {
            try {
                Writer writer = null;
                writer = new OutputStreamWriter(client.getOutputStream(), "UTF-8");
                String fName = type.split(";")[1];
                writer.write("2;" + fName + ";");
                writer.write("ends\n");
                writer.flush();

                DataInputStream inputStream = null;
                inputStream = getMessageStream();
                String basePath = "/sdcard/";
                int bufferSize = 8192;
                byte[] buf = new byte[bufferSize];
                int passedlen = 0;
                long len = 0;
                String fileName = inputStream.readUTF();
                String savePath = basePath + fileName;
                DataOutputStream fileOut = new DataOutputStream(new BufferedOutputStream(new BufferedOutputStream(new FileOutputStream(savePath))));
                len = inputStream.readLong();

                while (true) {
                    int read = 0;
                    if (inputStream != null) {
                        read = inputStream.read(buf);
                    }
                    passedlen += read;
                    if (read == -1) {
                        break;
                    }
                    CacheManager.putContent("jindu", fileName, String.valueOf((passedlen * 100 / len)), String.valueOf(len), String.valueOf(passedlen));
                    //下面进度条本为图形界面的prograssBar做的，这里如果是打文件，可能会重复打印出一些相同的百分比
                    fileOut.write(buf, 0, read);
                }

                fileOut.close();
                client.close();
                String path = basePath + fName;
                new SPUtils(ShowApplication.mApplication, "show").put("id", fName);
                ZipUtils.unzipFile(savePath, path);
                File f = new File(savePath);
                if (f.exists()) {
                    f.delete();
                }
                return "下载完成";
            } catch (Exception e) {
                return null;
            }
        }
        if (type.startsWith("6;")) {
            String cityName = type.split(";")[1];
            try {
                Writer writer = null;
                writer = new OutputStreamWriter(client.getOutputStream(), "UTF-8");
                writer.write("6;" + cityName);
                writer.write("ends\n");
                writer.flush();

                BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream(), "UTF-8"));
                StringBuffer sb = new StringBuffer();
                String temp;
                int index;
                while ((temp = br.readLine()) != null) {
                    if ((index = temp.indexOf("ends")) != -1) {
                        sb.append(temp.substring(0, index));
                        break;
                    }
                    sb.append(temp);
                }
                System.out.println("我是客户端,获取天气: " + sb);

                writer.close();
                br.close();
                client.close();
                return sb.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(type.startsWith("key;")){
            try {
                String mac=(String)map.get("mac");
                Writer writer = null;
                writer = new OutputStreamWriter(client.getOutputStream(),"UTF-8");
                writer.write("key;"+mac);
                writer.write("ends\n");
                writer.flush();

                BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream(),"UTF-8"));
                StringBuffer sb = new StringBuffer();
                String temp;
                int index;
                while ((temp=br.readLine()) != null) {
                    if ((index = temp.indexOf("ends")) != -1) {
                        sb.append(temp.substring(0, index));
                        break;
                    }
                    sb.append(temp);
                }
                System.out.println("我是客户端,获取日期时间: " + sb);

                writer.close();
                br.close();
                client.close();
                return sb.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    DataOutputStream out = null;
    DataInputStream getMessageStream = null;

    public void sendMessage(String sendMessage) throws Exception {
        try {
            out = new DataOutputStream(client.getOutputStream());
            if (sendMessage.equals("Windows")) {
                out.writeByte(0x1);
                out.flush();
                return;
            }
            if (sendMessage.equals("Unix")) {
                out.writeByte(0x2);
                out.flush();
                return;
            }
            if (sendMessage.equals("Linux")) {
                out.writeByte(0x3);
                out.flush();
            } else {
                out.writeUTF(sendMessage);
                out.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (out != null)
                out.close();
            throw e;
        } finally {
        }
    }

    public DataInputStream getMessageStream() throws Exception {
        try {
            getMessageStream = new DataInputStream(new BufferedInputStream(client.getInputStream()));
            return getMessageStream;
        } catch (Exception e) {
            e.printStackTrace();
            if (getMessageStream != null)
                getMessageStream.close();
            throw e;
        } finally {
        }
    }

    public void shutDownConnection() {
        try {
            if (out != null)
                out.close();
            if (getMessageStream != null)
                getMessageStream.close();
            if (client != null)
                client.close();
        } catch (Exception e) {

        }
    }
}





