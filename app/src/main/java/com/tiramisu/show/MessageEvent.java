package com.tiramisu.show;

import com.tiramisu.show.bean.EventBean;
import com.tiramisu.show.bean.WeatherBean;

public class MessageEvent { /* Additional fields if needed */
    private int type;
    private EventBean eventBean;

    private String pageId;
    public WeatherBean mWeatherBean;

    public EventBean getEventBean() {
        return eventBean;
    }

    public void setEventBean(EventBean eventBean) {
        this.eventBean = eventBean;
    }

    public MessageEvent(int type) {
        this.type = type;
    }

    public MessageEvent(int type, EventBean eventBean) {
        this.type = type;
        this.eventBean = eventBean;
    }

    public MessageEvent(int type, WeatherBean eventBean) {
        this.type = type;
        mWeatherBean = eventBean;
    }

    public MessageEvent(int type, String page) {
        this.type = type;
        pageId = page;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }
}