package com.tiramisu.show;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Util;
import com.tiramisu.show.util.ViewUtil;

import java.util.ArrayList;

/**
 * Created by 16010046 on 2017/4/21.
 */

public class ViewPagerArea extends AreaBean {
    private ViewPager mViewPager;
    private int mIndex;
    private int mTime;
    private ArrayList<String> mImages = new ArrayList<>();

    public void build(int index, RelativeLayout root, final AreaBean a, final Activity activity, final ArrayList<String> imgs) {
        mImages = imgs;
        final ViewPager v = new ViewPager(activity);
        ViewUtil.addView(root, v, a);
        v.setAdapter(new PagerAdapter() {
            @Override
            public Object instantiateItem(ViewGroup container, final int position) {
                ImageView view = new ImageView(activity);
                view.setScaleType(ImageView.ScaleType.FIT_XY);
                final int readPosition = position % imgs.size();
                if (!activity.isFinishing()) {
                    Glide.with(activity).load(imgs.get(readPosition)).into(view);
                }
                container.addView(view);
                setClick(view, a.clickTo);
                return view;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((View) object);
            }

            @Override
            public int getCount() {
                return imgs.size() > 1 ? Integer.MAX_VALUE : imgs.size();
            }

            @Override
            public boolean isViewFromObject(View view, Object o) {
                return view == o;
            }
        });
        mViewPager = v;
        mIndex = index;
        mTime = a.getTime();

    }

    public void handleMessage(Handler handler) {
        mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
        mTime = elementTimes.get(mViewPager.getCurrentItem() % mImages.size());
        handler.sendEmptyMessageDelayed(mIndex, mTime);
    }

    public void handleMessage(Handler handler, Message message) {
        if (mViewPager != null && mViewPager.getAdapter().getCount() > 1) {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
            if (mImages.size() > 0 && elementTimes.size() > 0) {
                mTime = elementTimes.get(mViewPager.getCurrentItem() % mImages.size()) * 1000;
            }
            handler.sendEmptyMessageDelayed(mIndex, mTime);
        }
    }
}
