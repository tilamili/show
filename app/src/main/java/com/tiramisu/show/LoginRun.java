package com.tiramisu.show;

import android.text.TextUtils;

import com.tiramisu.show.util.NetworkUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 16010046 on 2017/5/15.
 */

public class LoginRun implements Runnable {
    Map<String, String> map;

    public LoginRun(Map map) {
        this.map = map;
    }

    @Override
    public void run() {
        String sb = "";
        SocketOpen so = new SocketOpen();
        sb = so.getSocketLog(map, "0");
        if (sb.startsWith("error;")) {
            EventBus.getDefault().post(new MessageEvent(5));
        } else if (sb.startsWith("ok1;")) {
            EventBus.getDefault().post(new MessageEvent(6));
        } else if (sb.startsWith("ok2;")) {
            String proStr = sb.split(";")[2];
            String planStr = sb.split(";")[3];

            if (!TextUtils.isEmpty(proStr) && proStr.split("_").length > 0) {
                for (String proId : proStr.split("_")) {
                    if (!TextUtils.isEmpty(proId)) {
                        so.getSocketLog(map, "1;" + proId);
                    }
                }
            }
            if (!TextUtils.isEmpty(planStr)) {
                String planPath = "/sdcard/plan.xml";
                Map m = new HashMap();
                m.put("planContext", planStr);
                FileUtils.deleteFile(planPath);
                FileUtils.writeFileFromString(planPath, planStr, false);
            }
            EventBus.getDefault().post(new MessageEvent(7));
        } else if (sb.startsWith("ok5;")) {
            String temId = sb.split(";")[2];
            so.getSocketLog(map, "2;" + temId);
            EventBus.getDefault().post(new MessageEvent(6));
        } else {
            EventBus.getDefault().post(new MessageEvent(5));
        }
//        if (!sb.startsWith("error;")) {
//            getKey(map.get("IP"), map.get("shortName"), map.get("password"));
//        }
    }

    public static boolean writeFileContent(String filepath, String newstr) throws IOException {
        Boolean bool = false;
        String filein = newstr + "\r\n";//新写入的行，换行
        String temp = "";

        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        FileOutputStream fos = null;
        PrintWriter pw = null;
        try {
            File file = new File(filepath);//文件路径(包括文件名称)
            //将文件读入输入流
            fis = new FileInputStream(file);
            isr = new InputStreamReader(fis);
            br = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();

            //文件原有内容
            for (int i = 0; (temp = br.readLine()) != null; i++) {
                buffer.append(temp);
                // 行与行之间的分隔符 相当于“\n”
                buffer = buffer.append(System.getProperty("line.separator"));
            }
            buffer.append(filein);

            fos = new FileOutputStream(file);
            pw = new PrintWriter(fos);
            pw.write(buffer.toString().toCharArray());
            pw.flush();
            bool = true;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        } finally {
            //不要忘记关闭
            if (pw != null) {
                pw.close();
            }
            if (fos != null) {
                fos.close();
            }
            if (br != null) {
                br.close();
            }
            if (isr != null) {
                isr.close();
            }
            if (fis != null) {
                fis.close();
            }
        }
        return bool;
    }
}
