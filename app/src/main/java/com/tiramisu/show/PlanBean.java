package com.tiramisu.show;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by qf on 17/5/10.
 */

class PlanBean {

    /**
     * contentId : db4890704e824075bdcb5266131876d8
     * beginTime : 00:00:00
     * endTime : 22:00:00
     * dateType : 0
     * proDate : null
     * proWeek : 4
     */

    private String contentId;
    private String beginTime;
    private String endTime;
    private String dateType;
    private String proDate;
    private String proWeek;

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDateType() {
        return dateType;
    }

    public void setDateType(String dateType) {
        this.dateType = dateType;
    }

    public String getProDate() {
        return proDate;
    }

    public void setProDate(String proDate) {
        this.proDate = proDate;
    }

    public String getProWeek() {
        return proWeek;
    }

    public void setProWeek(String proWeek) {
        this.proWeek = proWeek;
    }

    public static List<PlanBean> parsePlan(String json) {
        List<PlanBean> planBeans = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(json);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject j = jsonArray.getJSONObject(i);
                PlanBean p = new PlanBean();
                p.beginTime = j.optString("beginTime");
                Log.d("holy", "PlanBean parsePlan() called with: " + "p.beginTime = [" + p.beginTime + "]");
                p.endTime = j.optString("endTime");
                p.dateType = j.optString("dateType");
                p.proDate = j.optString("proDate");
                p.proWeek = j.optString("proWeek");
                p.contentId = j.optString("contentId");
                planBeans.add(p);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return planBeans;
    }
}
