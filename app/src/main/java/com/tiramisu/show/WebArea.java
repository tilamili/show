package com.tiramisu.show;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.tiramisu.show.util.ViewUtil;

/**
 * Created by 16010046 on 2017/4/21.
 */

public class WebArea extends AreaBean {


    public void build(RelativeLayout root, AreaBean a, final Activity activity, String txtContent) {
        WebView webview = new WebView(activity);
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);  //支持js
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setSupportMultipleWindows(true);
        webview.setWebViewClient(new WebViewClient());
        webview.setWebChromeClient(new WebChromeClient());
        try {
//            String content = IOUtils.toString(activity.getAssets().open("error.html"))
//                    .replaceAll("%ERR_TITLE%", "bbq")
//                    .replaceAll("%ERR_DESC%", "ddr");
//
//            webview.loadDataWithBaseURL("file:///android_asset/error.html", content, "text/html", "UTF-8", null);
//            String content = IOUtils.toString(getAssets().open("help.html"))
//                    .replaceAll("%ERR_TITLE%", "sefwefefsf_FFFFFF_000000_PMingLiU_36_0_0__100_0_2_30_200_200")
//                    .replaceAll("%ERR_DESC%", "ddr");
//
//            webview.loadDataWithBaseURL("file:///android_asset/help.html", content, "text/html", "UTF-8", null);
        } catch (Exception e) {
        }
        ViewUtil.addView(root, webview, a);
        if (txtContent.startsWith("http")) {
            webview.loadUrl(txtContent);
        } else {
            webview.loadUrl("http://" + txtContent);
        }
        setClick(webview, a.clickTo);
    }

    public void handleMessage(Handler handler) {
    }

}
