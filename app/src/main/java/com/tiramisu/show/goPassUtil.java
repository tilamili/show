package com.tiramisu.show;


import android.text.TextUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class goPassUtil {
    public static String getPass(String date) {
        String dateStr = null;
        try {
            PassUtil des = new PassUtil("benziwangxin");//自定义密钥
            String bzStr = des.encrypt("benzi");
            if (!TextUtils.isEmpty(date) && date.split(bzStr).length > 0) {
                String y1g = des.decrypt(date.split(bzStr)[3]);
                String y2g = des.decrypt(date.split(bzStr)[5]);
                String y3g = des.decrypt(date.split(bzStr)[1]);
                String y4g = des.decrypt(date.split(bzStr)[7]);
                String yhg = des.decrypt(date.split(bzStr)[8]);
                String m1g = des.decrypt(date.split(bzStr)[6]);
                String m2g = des.decrypt(date.split(bzStr)[0]);
                String mhg = des.decrypt(date.split(bzStr)[9]);
                String d1g = des.decrypt(date.split(bzStr)[2]);
                String d2g = des.decrypt(date.split(bzStr)[4]);

                String mac1 = des.decrypt(date.split(bzStr)[10]);
                String mac2 = des.decrypt(date.split(bzStr)[11]);
                String mac3 = des.decrypt(date.split(bzStr)[12]);
                String mac4 = des.decrypt(date.split(bzStr)[13]);
                String mac5 = des.decrypt(date.split(bzStr)[14]);
                String mac6 = des.decrypt(date.split(bzStr)[15]);
                String y1 = String.valueOf(Integer.parseInt(y1g) - 9);
                String y2 = String.valueOf(Integer.parseInt(y2g) - 43);
                String y3 = y3g.split("z")[0];
                String y4 = y4g.split("ii")[0];
                String yh = yhg.split("y")[0];
                String m1 = m1g.split("qu")[0];
                String m2 = String.valueOf(Integer.parseInt(m2g) - 46);
                String mh = mhg.split("m")[0];
                String d1 = d1g.split("op")[0];
                String d2 = d2g.split("zm")[0];
                dateStr = y1 + y2 + y3 + y4 + yh + m1 + m2 + mh + d1 + d2 + "_" + mac1 + "-" + mac2 + "-" + mac3 + "-" + mac4 + "-" + mac5 + "-" + mac6;
            }

            return dateStr;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String readTxtFile(File fileName) throws Exception {
        String result = null;
        FileReader fileReader = null;
        BufferedReader bufferedReader = null;
        try {
            fileReader = new FileReader(fileName);
            bufferedReader = new BufferedReader(fileReader);
            try {
                String read = null;
                while ((read = bufferedReader.readLine()) != null) {
                    result = result + read;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            if (fileReader != null) {
                fileReader.close();
            }
        }
        return result;
    }
}
