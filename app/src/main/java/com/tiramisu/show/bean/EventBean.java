package com.tiramisu.show.bean;

/**
 * Created by 16010046 on 2017/5/16.
 */

public class EventBean {
    int type;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getPageX() {
        return pageX;
    }

    public void setPageX(String pageX) {
        this.pageX = pageX;
    }

    public String getPageY() {
        return pageY;
    }

    public void setPageY(String pageY) {
        this.pageY = pageY;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    public String getTextStyle() {
        return textStyle;
    }

    public void setTextStyle(String textStyle) {
        this.textStyle = textStyle;
    }

    String width;
    String height;
    String pageX;
    String pageY;
    String times;
    String textStyle;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String id;
}
