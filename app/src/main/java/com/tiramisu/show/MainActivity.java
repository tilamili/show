package com.tiramisu.show;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextClock;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tiramisu.show.bean.EventBean;
import com.tiramisu.show.bean.WeatherBean;
import com.tiramisu.show.media.IjkVideoView;
import com.tiramisu.show.service.MessageService;
import com.tiramisu.show.util.AlarmManagerUtil;
import com.tiramisu.show.util.FileLog;
import com.tiramisu.show.util.IOUtils;
import com.tiramisu.show.util.RestartAPPTool;
import com.tiramisu.show.util.SPUtils;
import com.tiramisu.show.util.ScreenUtils;
import com.tiramisu.show.util.ViewUtil;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.xmlpull.v1.XmlPullParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.tiramisu.show.util.AlarmManagerUtil.ALARM_ACTION;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "holy";
    static RelativeLayout root;
    HashMap<Integer, AreaBean> mAreaHash = new HashMap<>();
    public static HashMap<String, PlanBean> mPlanBeans = new HashMap<>();
    private SPUtils spUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        // 隐藏标题栏
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        // 隐藏状态栏
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        getWindow().setBackgroundDrawable(null);
//        getSupportActionBar().hide();
        Log.d("holy1", "界面创建" + this);
        spUtils = new SPUtils(this, "show");
        setContentView(R.layout.activity_main);
        root = (RelativeLayout) findViewById(R.id.activity_main);
        if (ShowApplication.isOutOfDate) {
            findViewById(R.id.out).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.out).setVisibility(View.GONE);

            AlarmManagerUtil.setAlarm(this, 1, 24, 00, 10000, 0, 0, "");
            new Task1().startSocket();
            if (!TextUtils.isEmpty(spUtils.getString("id"))) {
                String id = spUtils.getString("id");
//                makeView(id, 0);
            }
            if (spUtils.getBoolean("autoLogin")) {
                Intent serviceIntent = new Intent(this, MessageService.class);
                String ip = spUtils.getString("ip");
                String name = spUtils.getString("name");
                String pwd = spUtils.getString("pwd");
                serviceIntent.putExtra("ip", ip);
                serviceIntent.putExtra("name", name);
                serviceIntent.putExtra("pwd", pwd);
                startService(serviceIntent);
            }
        }
//        DigitalClock digitalClock=new DigitalClock(this);
//        TimeView timeView = new TimeView(this);

//        spUtils.put("id", "5028629025db4d679ad617835e94cae8");
//        makeView("5028629025db4d679ad617835e94cae7", 0);
//        spUtils.put("id", "1f360dc4095b4c809f63e500745223ea");
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(2000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                String sb = "3;1000;100;0;0;10;asdfasdf_FFFFFF_000000_PMingLiU_36_0_0__100_0_1_30";
//                String width = sb.toString().split(";")[1];
//                String height = sb.toString().split(";")[2];
//                String pageX = sb.toString().split(";")[3];
//                String pageY = sb.toString().split(";")[4];
//                String times = sb.toString().split(";")[5];
//                String textStyle = sb.toString().split(";")[6];
//                EventBean eventBean = new EventBean();
//                eventBean.setHeight(height);
//                eventBean.setWidth(width);
//                eventBean.setPageX(pageX);
//                eventBean.setPageY(pageY);
//                eventBean.setTimes(times);
//                eventBean.setTextStyle(textStyle);
//                eventBean.setType(2);
//            }
//        }).start();

//        String ip = NetworkUtils.getInstance(ShowApplication.mApplication).getNetworkIP();
//        String hostName = "qf";
//        String mac = NetworkUtils.getMacAddress();
//        FileLog.d("holy", "onCreate() called with: ip = [" + ip + "] mac = "+mac);
//        FileLog.d("holy", "onCreate() called with: ip = [" + IpUtil.getLocalIpAddress(this) + "] mac = "+ IpUtil.getLocalMacAddress(this));
//        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
//            @Override
//            public void onSystemUiVisibilityChange(int visibility) {
//                setHideVirtualKey(getWindow());
//            }
//        });

//        View decorView = getWindow().getDecorView();
//        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                | View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOptions);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            getWindow().addFlags(
//                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//            SystemBarTintManager tintManager = new SystemBarTintManager(this);
//            tintManager.setStatusBarTintColor(Color.TRANSPARENT);
//            tintManager.setStatusBarTintEnabled(true);
//        }
        Intent intent = new Intent();
        intent.putExtra("type", 0);
        intent.setAction(ALARM_ACTION);
        sendBroadcast(intent);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        root.postDelayed(new Runnable() {
            @Override
            public void run() {
                restartApplication();
            }
        }, 60 * 60 * 1000);
    }

    private void restartApplication() {
        Log.d("holy1", "重启");
//        final Intent intent = getPackageManager().getLaunchIntentForPackage(getPackageName());
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(intent);

//        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
//        manager.restartPackage(getPackageName());

        RestartAPPTool.restartAPP(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && Build.VERSION.SDK_INT >= 19) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    public void setHideVirtualKey(Window window) {
        //保持布局状态
        int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                //布局位于状态栏下方
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                //全屏
                View.SYSTEM_UI_FLAG_FULLSCREEN |
                //隐藏导航栏
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
        if (Build.VERSION.SDK_INT >= 19) {
            uiOptions |= 0x00001000;
        } else {
            uiOptions |= View.SYSTEM_UI_FLAG_LOW_PROFILE;
        }
        window.getDecorView().setSystemUiVisibility(uiOptions);
    }

    private void makeView(final String xmlPath, final int index) {
        FileLog.d("qiufeng", "makeView() called with: xmlPath = [" + xmlPath + "], index = [" + index + "]");
        SceneManage.getInstance().pageBeans.clear();
        try {
            File path = new File(Constant.DirShop,
                    xmlPath + "/xml/PROG/org.xml");
            FileInputStream fis = new FileInputStream(path);
            DomParserUtils.parserPage(fis);
            for (int i = 0; i < SceneManage.getInstance().pageBeans.size(); i++) {
                PageBean page = SceneManage.getInstance().pageBeans.get(i);
                File temp = new File(Constant.DirShop,
                        xmlPath + "/xml/PAGE" + page.getOrgId());
                FileInputStream tempStream = new FileInputStream(temp);
                ArrayList<AreaBean> areas = DomParserUtils.parserPageArea(tempStream);
                page.areaBeanArrayList.addAll(areas);
                File temp2 = new File(Constant.DirShop,
                        xmlPath + "/xml/PAGE" + page.getMainId());
                FileInputStream tempStream2 = new FileInputStream(temp2);
                long second = DomParserUtils.parserTime(tempStream2);
                page.setPageTime(second);
                for (int j = 0; j < areas.size(); j++) {
                    DomParserUtils.parserArea(xmlPath, areas.get(j));
                    DomParserUtils.parserAreaElement(xmlPath, areas.get(j));
                    DomParserUtils.parserElement(xmlPath, areas.get(j));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < SceneManage.getInstance().pageBeans.size(); i++) {

        }
        FileLog.d("qiufeng", "pagesize = [" + SceneManage.getInstance().pageBeans.size() + "]");
        if (SceneManage.getInstance().pageBeans.size() > 0) {
            int ii = index % SceneManage.getInstance().pageBeans.size();
            PageBean p = SceneManage.getInstance().pageBeans.get(ii);
            FileLog.d("qiufeng", "time = [" + p.getPageTime() + "]");
            if (p.getPageTime() != 0) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        removeAllView();
                        makeView(xmlPath, index + 1);
                    }
                }, p.getPageTime() * 1000);
            }
            for (int i = 0; i < p.areaBeanArrayList.size(); i++) {
                AreaBean a = p.areaBeanArrayList.get(i);
                FileLog.d("holy", "MainActivity onCreate() called with: " + "a = [" + a.getOrgId() + "]");
                FileLog.d("holy", "MainActivity onCreate() called with: " + i + " type = [" + a.getMaterialType() + "]");
                if (TextUtils.equals(a.getMaterialType(), Constant.TYPE_EXCEL) || TextUtils.equals(a.getMaterialType(), Constant.TYPE_IMG) || TextUtils.equals(a.getMaterialType(), Constant.TYPE_BARCODE)
                        || TextUtils.equals(a.getMaterialType(), Constant.TYPE_PDF) || TextUtils.equals(a.getMaterialType(), Constant.TYPE_DOC)) {
                    ViewPagerArea viewPagerArea = new ViewPagerArea();
                    viewPagerArea.build(i, root, a, this, a.elementUrls);
                    mAreaHash.put(i, viewPagerArea);
                    mHandler.sendEmptyMessageDelayed(i, a.getTime());
                }
                if (TextUtils.equals(a.getMaterialType(), Constant.TYPE_WEB)) {
                    WebArea webArea = new WebArea();
                    webArea.build(root, a, this, a.getTxtContent());
                    mAreaHash.put(i, webArea);
                    mHandler.sendEmptyMessageDelayed(i, a.getTime());
                }
                if (TextUtils.equals(a.getMaterialType(), Constant.TYPE_VIDEO) || TextUtils.equals(a.getMaterialType(), Constant.TYPE_STREAM) || TextUtils.equals(a.getMaterialType(), Constant.TYPE_PPT)) {
                    VideoArea.build(root, a, this, a.elementUrls);
                    mAreaHash.put(i, a);
                    mHandler.sendEmptyMessageDelayed(i, a.getTime());
                }
                if (TextUtils.equals(a.getMaterialType(), Constant.TYPE_TEXT)) {
                    TextArea textArea = new TextArea();
                    textArea.build(root, a, this, a.getTxtContent());
                    mAreaHash.put(i, textArea);
                    mHandler.sendEmptyMessageDelayed(i, a.getTime());
                }
                if (TextUtils.equals(a.getMaterialType(), Constant.TYPE_MUT)) {
                    MutArea mutArea = new MutArea();
                    mutArea.build(i, root, a, this);
                    mAreaHash.put(i, mutArea);
                    mHandler.sendEmptyMessageDelayed(i, a.getTime());
                }
                if (TextUtils.equals(a.getMaterialType(), Constant.TYPE_TIME)) {
                    TextClock textClock = new TextClock(this);
                    // 设置24时制显示格式
                    textClock.setFormat12Hour(null);
                    textClock.setFormat24Hour("yyyy:MM:dd aa h:mm");
                    ViewUtil.addView(root, textClock, a);
                }
                if (TextUtils.equals(a.getMaterialType(), Constant.TYPE_WEATHER)) {
                    final String cityName = a.getCityName();
                    final int width = a.getWidth().intValue();
                    final int height = a.getHeight().intValue();
                    final int topMargin = a.getY();
                    final int leftMargin = a.getX();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String ip = spUtils.getString("ip");
                            String name = spUtils.getString("name");
                            String pwd = spUtils.getString("pwd");
                            String weather = getWeather(ip, name, pwd, cityName);
                            if (!TextUtils.isEmpty(weather)) {
                                WeatherBean weatherBean = new WeatherBean();
                                weatherBean.h = height;
                                weatherBean.w = width;
                                weatherBean.x = leftMargin;
                                weatherBean.y = topMargin;
                                weatherBean.we = weather;
                                weatherBean.cityName = cityName;
                                EventBus.getDefault().post(new MessageEvent(EventConstant.EventWeather, weatherBean));
                            }
                        }
                    }).start();
                    FileLog.d("holy", "makeView() called with: xmlPath = [" + a.getCityName() + "]");
                }
            }
        }
    }

    public String getWeather(String ipAddr, String shortName, String password, String cityName) {
        String sb = "";
        if (!TextUtils.isEmpty(ipAddr) && !TextUtils.isEmpty(shortName) && !TextUtils.isEmpty(password)) {
            Map map = new HashMap();
            map.put("IP", ipAddr);
            map.put("shortName", shortName);
            map.put("password", password);
            map.put("port", "8899");
            SocketOpen so = new SocketOpen();
            sb = so.getSocketLog(map, "6;" + cityName);

        }
        return sb;
    }

    private void makeView(String xmlPath, String pageId) {
        FileLog.d("qiufeng", "makeView() called with: xmlPath = [" + xmlPath + "], pageId = [" + pageId + "]");
        SceneManage.getInstance().pageBeans.clear();
        try {
            File path = new File(Constant.DirShop,
                    xmlPath + "/xml/PROG/org.xml");
            FileInputStream fis = new FileInputStream(path);
            DomParserUtils.parserPage(fis);
            for (int i = 0; i < SceneManage.getInstance().pageBeans.size(); i++) {
                PageBean page = SceneManage.getInstance().pageBeans.get(i);
                File temp = new File(Constant.DirShop,
                        xmlPath + "/xml/PAGE" + page.getOrgId());
                FileInputStream tempStream = new FileInputStream(temp);
                ArrayList<AreaBean> areas = DomParserUtils.parserPageArea(tempStream);
                page.areaBeanArrayList.addAll(areas);
                for (int j = 0; j < areas.size(); j++) {
                    DomParserUtils.parserArea(xmlPath, areas.get(j));
                    DomParserUtils.parserAreaElement(xmlPath, areas.get(j));
                    DomParserUtils.parserElement(xmlPath, areas.get(j));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int j = 0; j < SceneManage.getInstance().pageBeans.size(); j++) {
            if (SceneManage.getInstance().pageBeans.get(j).getOrgId().contains(pageId)) {
                PageBean p = SceneManage.getInstance().pageBeans.get(j);
                for (int i = 0; i < p.areaBeanArrayList.size(); i++) {
                    AreaBean a = p.areaBeanArrayList.get(i);
                    FileLog.d("holy", "MainActivity onCreate() called with: " + "a = [" + a.getOrgId() + "]");
                    FileLog.d("holy", "MainActivity onCreate() called with: " + i + " type = [" + a.getMaterialType() + "]");
                    if (TextUtils.equals(a.getMaterialType(), Constant.TYPE_EXCEL) || TextUtils.equals(a.getMaterialType(), Constant.TYPE_IMG) || TextUtils.equals(a.getMaterialType(), Constant.TYPE_BARCODE)
                            || TextUtils.equals(a.getMaterialType(), Constant.TYPE_PDF) || TextUtils.equals(a.getMaterialType(), Constant.TYPE_PPT) || TextUtils.equals(a.getMaterialType(), Constant.TYPE_DOC)) {
                        ViewPagerArea viewPagerArea = new ViewPagerArea();
                        viewPagerArea.build(i, root, a, this, a.elementUrls);
                        mAreaHash.put(i, viewPagerArea);
                        mHandler.sendEmptyMessageDelayed(i, a.getTime());
                    }
                    if (TextUtils.equals(a.getMaterialType(), Constant.TYPE_WEB)) {
                        WebArea webArea = new WebArea();
                        webArea.build(root, a, this, a.getTxtContent());
                        mAreaHash.put(i, webArea);
                        mHandler.sendEmptyMessageDelayed(i, a.getTime());
                    }
                    if (TextUtils.equals(a.getMaterialType(), Constant.TYPE_VIDEO) || TextUtils.equals(a.getMaterialType(), Constant.TYPE_STREAM)) {
                        VideoArea.build(root, a, this, a.elementUrls);
                        mAreaHash.put(i, a);
                        mHandler.sendEmptyMessageDelayed(i, a.getTime());
                    }
                    if (TextUtils.equals(a.getMaterialType(), Constant.TYPE_TEXT)) {
                        TextArea textArea = new TextArea();
                        textArea.build(root, a, this, a.getTxtContent());
                        mAreaHash.put(i, textArea);
                        mHandler.sendEmptyMessageDelayed(i, a.getTime());
                    }
                    if (TextUtils.equals(a.getMaterialType(), Constant.TYPE_MUT)) {
                        MutArea mutArea = new MutArea();
                        mutArea.build(i, root, a, this);
                        mAreaHash.put(i, mutArea);
                        mHandler.sendEmptyMessageDelayed(i, a.getTime());
                    }
                }
                break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("holy1", "界面毁灭" + this);
        Intent intent = new Intent(this, MessageService.class);
        intent.putExtra("stop", 1);
        startService(intent);
        removeAllView();
    }

    public static void saveBitmap(Bitmap bm) {
        FileUtils.deleteFile("/sdcard/cap.jpg");
        Log.e(TAG, "保存图片");
        File f = new File("/sdcard/", "temp.jpg");
        if (f.exists()) {
            f.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(f);
            bm.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            Log.i(TAG, "已经保存");
            FileUtils.rename("/sdcard/temp.jpg", "cap.jpg");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != KeyEvent.KEYCODE_BACK) {
//            if (!ShowApplication.isOutOfDate) {
            Intent intent = new Intent(this, com.tiramisu.show.ui.LoginActivity.class);
            startActivity(intent);
//            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private Dialog showDialog(View v, int x, int y, int w, int h) {
        Dialog dialog = new Dialog(this);

        // setContentView可以设置为一个View也可以简单地指定资源ID
        // LayoutInflater
        // li=(LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
        // View v=li.inflate(R.layout.dialog_layout, null);
        // dialog.setContentView(v);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(v);
//        dialog.setTitle("Custom Dialog");

    /*
     * 获取圣诞框的窗口对象及参数对象以修改对话框的布局设置,
     * 可以直接调用getWindow(),表示获得这个Activity的Window
     * 对象,这样这可以以同样的方式改变这个Activity的属性.
     */
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setBackgroundDrawable(null);//去掉diaolog阴影
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.LEFT | Gravity.TOP);

        /*
         * lp.x与lp.y表示相对于原始位置的偏移.
         * 当参数值包含Gravity.LEFT时,对话框出现在左边,所以lp.x就表示相对左边的偏移,负值忽略.
         * 当参数值包含Gravity.RIGHT时,对话框出现在右边,所以lp.x就表示相对右边的偏移,负值忽略.
         * 当参数值包含Gravity.TOP时,对话框出现在上边,所以lp.y就表示相对上边的偏移,负值忽略.
         * 当参数值包含Gravity.BOTTOM时,对话框出现在下边,所以lp.y就表示相对下边的偏移,负值忽略.
         * 当参数值包含Gravity.CENTER_HORIZONTAL时
         * ,对话框水平居中,所以lp.x就表示在水平居中的位置移动lp.x像素,正值向右移动,负值向左移动.
         * 当参数值包含Gravity.CENTER_VERTICAL时
         * ,对话框垂直居中,所以lp.y就表示在垂直居中的位置移动lp.y像素,正值向右移动,负值向左移动.
         * gravity的默认值为Gravity.CENTER,即Gravity.CENTER_HORIZONTAL |
         * Gravity.CENTER_VERTICAL.
         *
         * 本来setGravity的参数值为Gravity.LEFT | Gravity.TOP时对话框应出现在程序的左上角,但在
         * 我手机上测试时发现距左边与上边都有一小段距离,而且垂直坐标把程序标题栏也计算在内了,
         * Gravity.LEFT, Gravity.TOP, Gravity.BOTTOM与Gravity.RIGHT都是如此,据边界有一小段距离
         */
        lp.x = x; // 新位置X坐标
        lp.y = y; // 新位置Y坐标
        lp.width = w; // 宽度
        lp.height = h; // 高度
        lp.dimAmount = 0f;//背景透明
//        lp.alpha = 0.7f; // 透明度

        // 当Window的Attributes改变时系统会调用此函数,可以直接调用以应用上面对窗口参数的更改,也可以用setAttributes
        // dialog.onWindowAttributesChanged(lp);
        dialogWindow.setAttributes(lp);

        /*
         * 将对话框的大小按屏幕大小的百分比设置
         */
//        WindowManager m = getWindowManager();
//        Display d = m.getDefaultDisplay(); // 获取屏幕宽、高用
//        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // 获取对话框当前的参数值
//        p.height = (int) (d.getHeight() * 0.6); // 高度设置为屏幕的0.6
//        p.width = (int) (d.getWidth() * 0.65); // 宽度设置为屏幕的0.65
//        dialogWindow.setAttributes(p);

        dialog.show();
        return dialog;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        FileLog.d("qiufeng", "收到消息" + event.getType());
        /* Do something */
        if (event.getType() == EventConstant.EventText) {
            EventBean eventBean = event.getEventBean();
            if (eventBean.getType() == 1) {
                //插播图片
                final ImageView imageView = new ImageView(this);
                Glide.with(this).load(eventBean.getTextStyle()).into(imageView);
//                root.addView(imageView);
//                RelativeLayout.LayoutParams l = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
//                l.width = Integer.valueOf(eventBean.getWidth());
//                l.height = Integer.valueOf(eventBean.getHeight());
//                l.topMargin = Integer.valueOf(eventBean.getPageY());
//                l.leftMargin = Integer.valueOf(eventBean.getPageX());
                final Dialog dialog = showDialog(imageView, Integer.valueOf(eventBean.getPageX()), Integer.valueOf(eventBean.getPageY()), Integer.valueOf(eventBean.getWidth()), Integer.valueOf(eventBean.getHeight()));
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                }, Integer.valueOf(eventBean.getTimes()) * 1000);
            } else if (eventBean.getType() == 2) {
                //插播文字
                final WebView webview = new WebView(this);
                WebSettings webSettings = webview.getSettings();
                webSettings.setJavaScriptEnabled(true);  //支持js
                webview.getSettings().setJavaScriptEnabled(true);
                webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                webview.getSettings().setSupportMultipleWindows(true);
                webview.setWebViewClient(new WebViewClient());
                webview.setWebChromeClient(new WebChromeClient());
                try {
//            String content = IOUtils.toString(activity.getAssets().open("error.html"))
//                    .replaceAll("%ERR_TITLE%", "bbq")
//                    .replaceAll("%ERR_DESC%", "ddr");
//
//            webview.loadDataWithBaseURL("file:///android_asset/error.html", content, "text/html", "UTF-8", null);

//                    "sefwefefsf_FFFFFF_000000_PMingLiU_36_0_0__100_0_2_30_200_200"
                    String content = IOUtils.toString(getAssets().open("help.html"))
                            .replaceAll("%ERR_TITLE%", eventBean.getTextStyle())
                            .replaceAll("%ERR_DESC%", "ddr");

                    webview.loadDataWithBaseURL("file:///android_asset/help.html", content, "text/html", "UTF-8", null);
                } catch (Exception e) {
                }
                final Dialog dialog = showDialog(webview, Integer.valueOf(eventBean.getPageX()), Integer.valueOf(eventBean.getPageY()), Integer.valueOf(eventBean.getWidth()), Integer.valueOf(eventBean.getHeight()));
//                root.addView(webview);
//                RelativeLayout.LayoutParams l = (RelativeLayout.LayoutParams) webview.getLayoutParams();
//                l.width = Integer.valueOf(eventBean.getWidth());
//                l.height = Integer.valueOf(eventBean.getHeight());
//                l.topMargin = Integer.valueOf(eventBean.getPageY());
//                l.leftMargin = Integer.valueOf(eventBean.getPageX());
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                }, Integer.valueOf(eventBean.getTimes()) * 1000);
            }
        } else if (event.getType() == EventConstant.EventClose) {
            mHandler.removeCallbacksAndMessages(null);
            removeAllView();
            MobclickAgent.onEvent(this, "event2");
        } else if (event.getType() == EventConstant.EventCap) {
            Bitmap b = ScreenUtils.captureWithStatusBar(this);
            saveBitmap(b);
        } else if (event.getType() == EventConstant.EventClick) {
            removeAllView();
            String fileName = spUtils.getString("id");
            makeView(fileName, event.getPageId());
        } else if (event.getType() == EventConstant.EventWeather) {
            TextView textView = new TextView(this);
            WeatherBean weatherBean = event.mWeatherBean;
            root.addView(textView);
            RelativeLayout.LayoutParams l = (RelativeLayout.LayoutParams) textView.getLayoutParams();
            l.width = weatherBean.w;
            l.height = weatherBean.h;
            l.topMargin = weatherBean.y;
            l.leftMargin = weatherBean.x;
            String we = weatherBean.we;
            if (we.split("_").length > 1) {
                System.out.println("++++" + we);
                String date = we.split("_")[1];
                String weather1 = we.split("_")[2];
                String wind = we.split("_")[3];
                String temperature = we.split("_")[4];
                textView.setText(weatherBean.cityName + " " + date + " " + weather1 + " " + wind + " " + temperature);
                textView.setBackgroundColor(Color.parseColor("#00000000"));
            }
        } else if (event.getType() == EventConstant.EventPlay) {
            removeAllView();
            EventBean eventBean = event.getEventBean();
            String fileName = eventBean.getId();
            makeView(fileName, 0);
        } else if (event.getType() == EventConstant.EventPlan) {
            Log.d("qiufeng", "收到plan消息");
            Intent intent = new Intent();
            intent.setAction("com.tiramisu.show");
            sendBroadcast(intent);
        }
    }

    private void removeAllView() {
        if (root == null) {
            return;
        }
        int count = root.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = root.getChildAt(i);
            if (view instanceof IjkVideoView) {
                IjkVideoView temp = (IjkVideoView) view;
                temp.stop();
            }
        }
//        for (int i = 0; i < count; i++) {
//            View view = root.getChildAt(i);
//            root.removeView(view);
//        }
        root.removeAllViews();
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            mAreaHash.get(msg.what).handleMessage(this, msg);
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    private void readPageMain(String mainName) {
        try {
            File path = new File(Environment.getExternalStorageDirectory(),
                    "hor/xml/PAGE/" + mainName);
            FileInputStream fis = new FileInputStream(path);

            // 获得pull解析器对象
            XmlPullParser parser = Xml.newPullParser();
            // 指定解析的文件和编码格式
            parser.setInput(fis, "utf-8");

            int eventType = parser.getEventType(); // 获得事件类型
            ProgBean p = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagName = parser.getName(); // 获得当前节点的名称

                switch (eventType) {
                    case XmlPullParser.START_TAG: // 当前等于开始节点 <person>
                        if (tagName.startsWith("pagemain")) { // <persons>
                            p = new ProgBean();
                            p.setId(parser.nextText());
                            SceneManage.getInstance().progBeans.add(p);
                        } else if (tagName.startsWith("pageorg")) { // <person id="1">
                            p.setName(parser.nextText());
                        }
                        break;
                    case XmlPullParser.END_TAG: // </persons>
                        break;
                    default:
                        break;
                }
                eventType = parser.next(); // 获得下一个事件类型
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }

    private void readPageArea(String orgName) {
        try {
            File path = new File(Environment.getExternalStorageDirectory(),
                    "hor/xml/PAGE/" + orgName);
            FileInputStream fis = new FileInputStream(path);

            // 获得pull解析器对象
            XmlPullParser parser = Xml.newPullParser();
            // 指定解析的文件和编码格式
            parser.setInput(fis, "utf-8");

            int eventType = parser.getEventType(); // 获得事件类型
            ProgBean p = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagName = parser.getName(); // 获得当前节点的名称

                switch (eventType) {
                    case XmlPullParser.START_TAG: // 当前等于开始节点 <person>
                        if (tagName.startsWith("pagemain")) { // <persons>
                            p = new ProgBean();
                            p.setId(parser.nextText());
                            SceneManage.getInstance().progBeans.add(p);
                        } else if (tagName.startsWith("pageorg")) { // <person id="1">
                            p.setName(parser.nextText());
                        }
                        break;
                    case XmlPullParser.END_TAG: // </persons>
                        break;
                    default:
                        break;
                }
                eventType = parser.next(); // 获得下一个事件类型
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }

    /**
     * 得到Assets里面相应的文件流
     *
     * @param fileName
     * @return
     */
    private InputStream getAssetsStream(String fileName) {
        InputStream is = null;
        try {
            is = getAssets().open(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return is;
    }

    /**
     * 解析test3.xml
     *
     * @param is
     * @return list
     */
    private List<ClassBean> parseFile3(InputStream is) {
        List<ClassBean> list = null;
        ClassBean bean = null;
        List<StudentBean> sList = null;
        StudentBean be = null;
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(is, "utf-8");
            int event = parser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                switch (event) {
                    case XmlPullParser.START_DOCUMENT:
                        list = new ArrayList<ClassBean>();
                        break;
                    case XmlPullParser.START_TAG:
                        if ("class".equals(parser.getName())) {
                            bean = new ClassBean();
                            bean.setId(parser.getAttributeValue(0));
                            bean.setName(parser.getAttributeValue(1));
                            sList = new ArrayList<StudentBean>();
                        } else if ("student".equals(parser.getName())) {
                            be = new StudentBean();
                            be.setId(parser.getAttributeValue(0));
                            sList.add(be);
                        } else if ("name".equals(parser.getName())) {
                            be.setName(parser.nextText());
                        } else if ("sex".equals(parser.getName())) {
                            be.setSex(parser.nextText());
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        if ("class".equals(parser.getName())) {
                            bean.setList(sList);
                            list.add(bean);
                            bean = null;
                        }
                        break;
                }
                event = parser.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 读取SD卡中的XML文件,使用pull解析
     *
     * @param fileName
     */
    public void readxml(String fileName) {
        try {
            File path = new File(Environment.getExternalStorageDirectory(),
                    fileName);
            FileInputStream fis = new FileInputStream(path);

            // 获得pull解析器对象
            XmlPullParser parser = Xml.newPullParser();
            // 指定解析的文件和编码格式
            parser.setInput(fis, "utf-8");

            int eventType = parser.getEventType(); // 获得事件类型
            String id = null;
            String name = null;
            String gender = null;
            String age = null;
            ProgBean p = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagName = parser.getName(); // 获得当前节点的名称

                switch (eventType) {
                    case XmlPullParser.START_TAG: // 当前等于开始节点 <person>
                        if (tagName.startsWith("pagemain")) { // <persons>
                            p = new ProgBean();
                            p.setId(parser.nextText());
                            SceneManage.getInstance().progBeans.add(p);
                        } else if (tagName.startsWith("pageorg")) { // <person id="1">
                            p.setName(parser.nextText());
                        }
                        break;
                    case XmlPullParser.END_TAG: // </persons>
                        break;
                    default:
                        break;
                }
                eventType = parser.next(); // 获得下一个事件类型
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }
}
