package com.tiramisu.show;

/**
 * Created by 16010046 on 2017/4/27.
 */

public class Constant {
    public static final String TYPE_IMG = "10390001";
    public static final String TYPE_VIDEO = "10390002";

    public static final String TYPE_TEXT = "10390004";
    public static final String TYPE_DOC = "10390005";
    public static final String TYPE_EXCEL = "10390006";
    public static final String TYPE_PPT = "10390007";
    public static final String TYPE_BARCODE = "10390008";
    public static final String TYPE_PDF = "10390009";
    public static final String TYPE_WEB = "10390010";
    public static final String TYPE_STREAM = "10390011";
    public static final String TYPE_MUT = "10390021";
    public static final String TYPE_WEATHER = "10390022";
    public static final String TYPE_TIME = "10390023";

    public static final String DirShop = "/sdcard/ShopDir";
    public static final int EVENTTYPE_WEATHER = 11;

}
