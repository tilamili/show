package com.tiramisu.show;

import java.util.ArrayList;

/**
 * Created by 16010046 on 2017/4/18.
 */

public class SceneManage {
    private static SceneManage mSceneManage;

    public static SceneManage getInstance() {
        if (mSceneManage == null) {
            mSceneManage = new SceneManage();
        }
        return mSceneManage;
    }

    public ArrayList<ProgBean> progBeans = new ArrayList<>();
    public ArrayList<PageBean> pageBeans = new ArrayList<>();
}
