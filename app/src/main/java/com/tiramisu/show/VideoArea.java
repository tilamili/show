package com.tiramisu.show;

import android.app.Activity;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;

import com.tiramisu.show.media.AndroidMediaController;
import com.tiramisu.show.media.IjkVideoView;
import com.tiramisu.show.util.ViewUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import tv.danmaku.ijk.media.player.IMediaPlayer;


/**
 * Created by 16010046 on 2017/4/21.
 */

public class VideoArea {

    public static void build(RelativeLayout root, final AreaBean a, final Activity activity, final ArrayList<String> imgs) {
        final IjkVideoView videoView = new IjkVideoView(activity);
//        videoView.setBackgroundResource(R.color.colorAccent);
        AndroidMediaController controller = new AndroidMediaController(activity, false);
        videoView.setMediaController(controller);
//        String url = "https://wdl.wallstreetcn.com/41aae4d2-390a-48ff-9230-ee865552e72d";
        String url = "";
        if (imgs.size() > 0) {
            url = imgs.get(0);
        }
        // String url = "http://o6wf52jln.bkt.clouddn.com/演员.mp3";
        Log.d("holy", "build() called with: root = [" + url + "]");
//        videoView.setVideoURI(Uri.parse(url));
        videoView.setVideoURIS(imgs);
//        videoView.setVideoURI(Uri.parse("/sdcard/k.avi"));
        videoView.start();
//        videoView.setOnCompletionListener(new IMediaPlayer.OnCompletionListener() {
//            @Override
//            public void onCompletion(IMediaPlayer mp) {
//                videoView.start();
//            }
//        });
        if (!TextUtils.isEmpty(a.clickTo)) {
            videoView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    EventBus.getDefault().post(new MessageEvent(EventConstant.EventClick, a.clickTo));
                    return false;
                }
            });
        }
        ViewUtil.addView(root,videoView,a);
    }
}
