package com.tiramisu.show;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 用来处理Socket请求的
 */
public class Task1 {
    //socket server 线程
    private SocketThread socketThread;

    public void startSocket() {
        if (null == socketThread) {
            //新建线程类
            socketThread = new SocketThread(null);
            //启动线程
            socketThread.start();
        }
    }

    public void closeSocket() {
        socketThread.closeSocketServer();
        socketThread.interrupt();
    }

}


class SocketThread extends Thread {
    private ServerSocket serverSocket = null;

    public SocketThread(ServerSocket serverScoket) {
        try {
            if (null == serverSocket) {
                this.serverSocket = new ServerSocket(8898);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void run() {
        while (!this.isInterrupted()) {
            try {
                Socket socket = serverSocket.accept();

                if (null != socket && !socket.isClosed()) {
                    //处理接受的数据
                    new SocketOperate(socket).start();
                }
                socket.setSoTimeout(30000);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void closeSocketServer() {
        try {
            if (null != serverSocket && !serverSocket.isClosed()) {
                serverSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
