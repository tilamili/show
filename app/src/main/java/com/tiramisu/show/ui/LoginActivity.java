package com.tiramisu.show.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.tiramisu.show.FileUtils;
import com.tiramisu.show.MainActivity;
import com.tiramisu.show.MessageEvent;
import com.tiramisu.show.R;
import com.tiramisu.show.SocketOpen;
import com.tiramisu.show.service.MessageService;
import com.tiramisu.show.util.SPUtils;
import com.tiramisu.show.util.ScreenUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

import static com.tiramisu.show.MainActivity.saveBitmap;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    EditText textIp;
    EditText textName;
    EditText textPwd;
    private SPUtils spUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 隐藏标题栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // 隐藏状态栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setBackgroundDrawable(null);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);
        textIp = (EditText) findViewById(R.id.ip);
        textName = (EditText) findViewById(R.id.name);
        textPwd = (EditText) findViewById(R.id.pwd);
        spUtils = new SPUtils(this, "show");
        String ip = spUtils.getString("ip");
        textIp.setText(ip);
        String name = spUtils.getString("name");
        textName.setText(name);
        String pwd = spUtils.getString("pwd");
        textPwd.setText(pwd);
    }

    public void connect(View view) {
        Intent serviceIntent = new Intent(this, MessageService.class);
        serviceIntent.putExtra("ip", textIp.getText().toString());
        serviceIntent.putExtra("name", textName.getText().toString());
        serviceIntent.putExtra("pwd", textPwd.getText().toString());
        startService(serviceIntent);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        Log.d(TAG, "onMessageEvent() called with: event = [" + event + "]");
        /* Do something */
        if (event.getType() == 5) {
            Toast.makeText(this, "登录失败", Toast.LENGTH_LONG).show();
        } else if (event.getType() == 6) {
            spUtils.put("autoLogin", true);
            spUtils.put("ip", textIp.getText().toString());
            spUtils.put("name", textName.getText().toString());
            spUtils.put("pwd", textPwd.getText().toString());
            Toast.makeText(this, "登录成功", Toast.LENGTH_LONG).show();
            finish();
        } else if (event.getType() == 7) {
            spUtils.put("autoLogin", true);
            spUtils.put("ip", textIp.getText().toString());
            spUtils.put("name", textName.getText().toString());
            spUtils.put("pwd", textPwd.getText().toString());
            finish();
        }
    }
}