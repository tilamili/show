package com.tiramisu.show;

/**
 * Created by qf on 18/4/10.
 */

public interface EventConstant {
    int EventPlay = 100;
    int EventClose = 2;
    int EventCap = 3;
    int EventText = 0;
    int EventClick = 8;
    int EventPlan = 7;
    int EventWeather = 11;
}
