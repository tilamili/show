package com.tiramisu.show.element;

/**
 * Created by qf on 17/5/17.
 */

public class BaseElement {
    public int height;
    public int width;
    public int x;
    public int y;
    public long time;
}
