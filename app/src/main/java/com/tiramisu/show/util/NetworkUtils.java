package com.tiramisu.show.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author xiongchengguang
 * @ClassName NetorkUtils
 * @Descripton 网络工具类
 * @date 2016年5月16日上午11:21:08
 */
public class NetworkUtils {

    public static final int TYPE_AP = 0;
    public static final int TYPE_WIFI = 1;
    public static final int TYPE_NULL = -1;
    public static final int TYPE_ETHERNET = 9;

    public static final String NETWORK_TYPE = "network_type";
    public static final String NETWORK_IP = "network_ip";
    public static final String NETWORK_MAC = "network_mac";
    public static final String NETWORK_NAME = "network_name";

    private static WifiManager wm;
    private static ConnectivityManager cm;
    public static final String TAG = "NetorkUtils";
    private static NetworkUtils instance = null;
    private static Context context;


    public static final String WLAN_PARAMETER = "wlan0";

    public static final String EHTERNET_PARAMETER = "eth0";

    public static NetworkUtils getInstance(Context context) {
        NetworkUtils.context = context;
        initPlatform();
        if (instance == null) {
            synchronized (NetworkUtils.class) {
                if (instance == null) {
                    instance = new NetworkUtils();
                }
            }
        }
        return instance;
    }

    private static void initPlatform() {
        if (wm == null) {
            wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        }

        if (cm == null) {
            cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        }
    }

    public int getNetworkType() {
        if (isWifiApEnabled()) {
            return TYPE_AP;
        } else {
            NetworkInfo info = cm.getActiveNetworkInfo();
            if (info != null && info.isConnected()) {
                if (info.getType() == ConnectivityManager.TYPE_ETHERNET) {
                    return TYPE_ETHERNET;
                } else if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                    return TYPE_WIFI;
                } else {
                    return TYPE_NULL;
                }
            } else {
                return TYPE_NULL;
            }
        }
    }

    public String getNetworkName() {
        String ssid = "net unknown";
        int type = getNetworkType();
        switch (type) {
            case TYPE_AP:
                ssid = getNetworkSSID();
                break;
            case TYPE_WIFI:
                ssid = wm.getConnectionInfo().getSSID();
                break;
            case TYPE_ETHERNET:
                ssid = "有线网络";
                break;
            case TYPE_NULL:
                ssid = "未知网络";
                break;

            default:
                break;
        }

        return ssid;
    }

    public String getNetworkIP() {
        String address = "";
        int type = getNetworkType();
        switch (type) {
            case TYPE_AP:
                address = "192.168.43.1";
                break;
            case TYPE_WIFI:
                int ip = wm.getConnectionInfo().getIpAddress();
                address = ((ip & 0xff) + "." + (ip >> 8 & 0xff) + "." + (ip >> 16 & 0xff) + "." + (ip >> 24 & 0xff));
                break;
            case TYPE_ETHERNET:
                address = localAddress();
                break;
            case TYPE_NULL:
                break;

            default:
                break;
        }
        return address;
    }

    public static String getMacAddress(Context context) {
 /*获取mac地址有一点需要注意的就是android 6.0版本后，以下注释方法不再适用，不管任何手机都会返回"02:00:00:00:00:00"这个默认的mac地址，这是googel官方为了加强权限管理而禁用了getSYstemService(Context.WIFI_SERVICE)方法来获得mac地址。*/
        //        String macAddress= "";
//        WifiManager wifiManager = (WifiManager) MyApp.getContext().getSystemService(Context.WIFI_SERVICE);
//        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
//        macAddress = wifiInfo.getMacAddress();
//        return macAddress;
//
//        String macAddress = null;
//        StringBuffer buf = new StringBuffer();
//        NetworkInterface networkInterface = null;
//        try {
//            networkInterface = NetworkInterface.getByName("eth1");
//            if (networkInterface == null) {
//                networkInterface = NetworkInterface.getByName("wlan0");
//            }
//            if (networkInterface == null) {
//                return "02:00:00:00:00:02";
//            }
//            byte[] addr = networkInterface.getHardwareAddress();
//            for (byte b : addr) {
//                buf.append(String.format("%02X:", b));
//            }
//            if (buf.length() > 0) {
//                buf.deleteCharAt(buf.length() - 1);
//            }
//            macAddress = buf.toString();
//        } catch (SocketException e) {
//            e.printStackTrace();
//            return "02:00:00:00:00:02";
//        }
//        return macAddress;

        int type = getNetWorkStatus(context);
        if (type == ConnectivityManager.TYPE_ETHERNET) {
            return getEthernetMACAddress();
        } else if (type == ConnectivityManager.TYPE_WIFI) {
            return getWIFIAddress();
        }
        return "";
    }

    public String getNetworkMac() {
        String mac = "";
        int type = getNetworkType();
        switch (type) {
            case TYPE_AP:
                mac = wm.getConnectionInfo().getMacAddress();
                if (mac == null) {
                    mac = UUID.randomUUID().toString();
                }
                break;
            case TYPE_WIFI:
                mac = wm.getConnectionInfo().getMacAddress();
                if (mac == null) {
                    mac = UUID.randomUUID().toString();
                }
                break;
            case TYPE_ETHERNET:
                mac = localMac();
                break;
            case TYPE_NULL:
                break;

            default:
                break;
        }
        return mac.toUpperCase();
    }

    public static int getNetWorkStatus(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return networkInfo.getType();
        }

        return -1;
    }

    public static String getEthernetMACAddress() {
        return getMACAddress(EHTERNET_PARAMETER);
    }

    public static String getWIFIAddress() {
        return getMACAddress(WLAN_PARAMETER);
    }

    /**
     * Returns MAC address of the given interface name.
     *
     * @param interfaceName eth0, wlan0 or NULL=use first interface
     * @return mac address or empty string
     */
    public static String getMACAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections
                    .list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) {
                        continue;
                    }
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null) {
                    return "";
                }
                StringBuilder buf = new StringBuilder();
                for (int idx = 0; idx < mac.length; idx++) {
                    buf.append(String.format("%02X:", mac[idx]));
                }
                if (buf.length() > 0) {
                    buf.deleteCharAt(buf.length() - 1);
                }
                return buf.toString();
            }
        } catch (Exception ex) {
        } // ignore exceptions

        return "";
    }


    private String getNetworkSSID() {
        try {
            Method method = wm.getClass().getMethod("getWifiApConfiguration");
            method.setAccessible(true);
            WifiConfiguration configuration = (WifiConfiguration) method.invoke(wm);
            return configuration.SSID;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "net unknown";
    }

    public boolean isWifiApEnabled() {
        try {
            Method method = wm.getClass().getMethod("isWifiApEnabled");
            method.setAccessible(true);
            return (Boolean) method.invoke(wm);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public String loadReadFile(String path) {
        StringBuffer sb = new StringBuffer();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            char[] buf = new char[8192];
            int len = 0;
            while ((len = reader.read(buf)) != -1) {
                String str = String.valueOf(buf, 0, len);
                sb.append(str);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public String localAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    Pattern p = Pattern.compile("^[1-9][0-9]*.[0-9][0-9]*.[0-9][0-9]*.[0-9][0-9]*$");
                    Matcher m = p.matcher(inetAddress.getHostAddress().toString());
                    if (!inetAddress.isLoopbackAddress() && m.matches()) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String localMac() {
        try {
            return loadReadFile("/sys/class/net/eth0/address").toUpperCase().substring(0, 17);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Map getAllNetworkInfo() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(NETWORK_TYPE, getNetworkType());
        map.put(NETWORK_IP, getNetworkIP());
        map.put(NETWORK_MAC, getNetworkMac());
        map.put(NETWORK_NAME, getNetworkName());
        return map;
    }

    public void startAP() {
        if (wm.isWifiEnabled()) {
            startAP(getNetworkSSID(), "12345678");
        }
    }

    private void startAP(String ssid, String password) {
        Method method1 = null;
        try {
            method1 = wm.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
            WifiConfiguration netConfig = new WifiConfiguration();

            netConfig.SSID = ssid;
            netConfig.preSharedKey = password;

            netConfig.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
            netConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
            netConfig.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
            netConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            method1.invoke(wm, netConfig, true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}