package com.tiramisu.show.util;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tiramisu.show.AreaBean;
import com.tiramisu.show.media.AndroidMediaController;
import com.tiramisu.show.media.IjkVideoView;

import java.util.ArrayList;

/**
 * Created by qf on 17/5/17.
 */

public class ViewUtil {

    public static ViewPager buildViewPager(RelativeLayout root, AreaBean a, final Activity activity, final ArrayList<String> imgs) {
        ViewPager v = new ViewPager(activity);
        root.addView(v);
        RelativeLayout.LayoutParams l = (RelativeLayout.LayoutParams) v.getLayoutParams();
        l.width = a.getWidth().intValue();
        l.height = a.getHeight().intValue();
        l.topMargin = a.getY();
        l.leftMargin = a.getX();
        v.setAdapter(new PagerAdapter() {
            @Override
            public Object instantiateItem(ViewGroup container, final int position) {
                ImageView view = new ImageView(activity);
                view.setScaleType(ImageView.ScaleType.FIT_XY);
                final int readPosition = position % imgs.size();
                Glide.with(activity).load(imgs.get(readPosition)).into(view);
                container.addView(view);
                return view;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((View) object);
            }

            @Override
            public int getCount() {
                return Integer.MAX_VALUE;
            }

            @Override
            public boolean isViewFromObject(View view, Object o) {
                return view == o;
            }
        });
        return v;
    }

    public static WebView buildWebView(RelativeLayout root, AreaBean a, final Activity activity, String txtContent) {
        WebView webview = new WebView(activity);
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);  //支持js
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setSupportMultipleWindows(true);
        webview.setWebViewClient(new WebViewClient());
        webview.setWebChromeClient(new WebChromeClient());
        try {
//            String content = IOUtils.toString(activity.getAssets().open("error.html"))
//                    .replaceAll("%ERR_TITLE%", "bbq")
//                    .replaceAll("%ERR_DESC%", "ddr");
//
//            webview.loadDataWithBaseURL("file:///android_asset/error.html", content, "text/html", "UTF-8", null);
//            String content = IOUtils.toString(getAssets().open("help.html"))
//                    .replaceAll("%ERR_TITLE%", "sefwefefsf_FFFFFF_000000_PMingLiU_36_0_0__100_0_2_30_200_200")
//                    .replaceAll("%ERR_DESC%", "ddr");
//
//            webview.loadDataWithBaseURL("file:///android_asset/help.html", content, "text/html", "UTF-8", null);
        } catch (Exception e) {
        }
        root.addView(webview);
        if (txtContent.startsWith("http")) {
            webview.loadUrl(txtContent);
        } else {
            webview.loadUrl("http://" + txtContent);
        }
        RelativeLayout.LayoutParams l = (RelativeLayout.LayoutParams) webview.getLayoutParams();
        l.width = a.getWidth().intValue();
        l.height = a.getHeight().intValue();
        l.topMargin = a.getY();
        l.leftMargin = a.getX();
        return webview;
    }

    public static TextView buildTextView(RelativeLayout root, AreaBean a, final Activity activity, String txtContent) {
        TextView textView = new TextView(activity);
        textView.setText(txtContent);
        root.addView(textView);
        RelativeLayout.LayoutParams l = (RelativeLayout.LayoutParams) textView.getLayoutParams();
        l.width = a.getWidth().intValue();
        l.height = a.getHeight().intValue();
        l.topMargin = a.getY();
        l.leftMargin = a.getX();
        return textView;
    }

    public static ImageView buildImageView(RelativeLayout root, AreaBean a, final Activity activity, String txtContent) {
        ImageView imageView = new ImageView(activity);
        Glide.with(activity).load(txtContent).into(imageView);
        root.addView(imageView);
        RelativeLayout.LayoutParams l = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
        l.width = a.getWidth().intValue();
        l.height = a.getHeight().intValue();
        l.topMargin = a.getY();
        l.leftMargin = a.getX();
        return imageView;
    }

    public static IjkVideoView buildVideoView(RelativeLayout root, AreaBean a, final Activity activity, String url) {
        IjkVideoView videoView = new IjkVideoView(activity);
//        videoView.setBackgroundResource(R.color.colorAccent);
        AndroidMediaController controller = new AndroidMediaController(activity, false);
        videoView.setMediaController(controller);
        videoView.setVideoURI(Uri.parse(url));

        videoView.start();
        root.addView(videoView);
        RelativeLayout.LayoutParams l = (RelativeLayout.LayoutParams) videoView.getLayoutParams();
        l.width = a.getWidth().intValue();
        l.height = a.getHeight().intValue();
        l.topMargin = a.getY();
        l.leftMargin = a.getX();
        return videoView;
    }

    public static void addView(RelativeLayout root, View child, AreaBean a) {
        if (!TextUtils.isEmpty(a.getBgColor())) {
            String bg = a.getBgColor();
            if (!a.getBgColor().startsWith("#")) {
                bg = "#" + a.getBgColor();
            }
            if (child instanceof WebView) {
                child.setBackgroundColor(Color.TRANSPARENT);
            } else {
                child.setBackgroundColor(Color.parseColor(bg));
            }
        }
        root.addView(child);
        RelativeLayout.LayoutParams l = (RelativeLayout.LayoutParams) child.getLayoutParams();
        l.width = a.getWidth().intValue();
        l.height = a.getHeight().intValue();
        l.topMargin = a.getY();
        l.leftMargin = a.getX();
        Log.d("holy", "addView [" + child + "], l = [" + l.width + "] h = " + l.height + " y = " + l.topMargin + " x = " + l.leftMargin);
    }
}
