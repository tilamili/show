package com.tiramisu.show;

import android.text.TextUtils;
import android.util.Log;

import com.tiramisu.show.element.ImageElement;
import com.tiramisu.show.element.TextElement;
import com.tiramisu.show.element.VideoElement;
import com.tiramisu.show.element.ViewPagerElement;
import com.tiramisu.show.element.WebElement;
import com.tiramisu.show.util.ScreenUtils;
import com.tiramisu.show.util.TimeUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class DomParserUtils {
    public static List<Person> parserXmlByDom(InputStream inputStream) throws Exception {
        List<Person> persons = new ArrayList<Person>();
        //    得到一个DocumentBuilderFactory解析工厂类
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        //    得到一个DocumentBuilder解析类
        DocumentBuilder builder = factory.newDocumentBuilder();
        //    接收一个xml的字符串来解析xml,Document代表整个xml文档
        Document document = builder.parse(inputStream);
        //    得到xml文档的根元素节点
        Element personsElement = document.getDocumentElement();
        //    得到标签为person的Node对象的集合NodeList
        NodeList nodeList = personsElement.getElementsByTagName("person");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Person person = new Person();
            //    如果该Node是一个Element
            if (nodeList.item(i).getNodeType() == Document.ELEMENT_NODE) {
                Element personElement = (Element) nodeList.item(i);
                //    得到id的属性值
                String id = personElement.getAttribute("id");
                person.setId(Integer.parseInt(id));

                //    得到person元素下的子元素
                NodeList childNodesList = personElement.getChildNodes();
                for (int j = 0; j < childNodesList.getLength(); j++) {
                    if (childNodesList.item(j).getNodeType() == Document.ELEMENT_NODE) {
                        //    解析到了person下面的name标签
                        if ("name".equals(childNodesList.item(j).getNodeName())) {
                            //    得到name标签的文本值
                            String name = childNodesList.item(j).getFirstChild().getNodeValue();
                            person.setName(name);
                        } else if ("address".equals(childNodesList.item(j).getNodeName())) {
                            String age = childNodesList.item(j).getFirstChild().getNodeValue();
                            person.setAge(Integer.parseInt(age));
                        }
                    }
                }

                persons.add(person);
                person = null;
            }
        }
        return persons;
    }


    public static void parserPage(InputStream inputStream) throws Exception {
        //    得到一个DocumentBuilderFactory解析工厂类
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        //    得到一个DocumentBuilder解析类
        DocumentBuilder builder = factory.newDocumentBuilder();
        //    接收一个xml的字符串来解析xml,Document代表整个xml文档
        Document document = builder.parse(inputStream);
        //    得到xml文档的根元素节点
        Element personsElement = document.getDocumentElement();
        //    得到标签为person的Node对象的集合NodeList
        NodeList nodeList = personsElement.getElementsByTagName("pages");
        for (int i = 0; i < nodeList.getLength(); i++) {
            //    如果该Node是一个Element
            if (nodeList.item(i).getNodeType() == Document.ELEMENT_NODE) {
                Element personElement = (Element) nodeList.item(i);
                NodeList childNodesList = personElement.getElementsByTagName("count");
                String count = childNodesList.item(0).getFirstChild().getNodeValue();
                int size = Integer.valueOf(count);
                for (int j = 0; j < size; j++) {
                    PageBean page = new PageBean();
                    NodeList tempMain = personElement.getElementsByTagName("pagemain" + j);
                    String pagemain = tempMain.item(0).getFirstChild().getNodeValue();
                    page.setMainId(pagemain);
                    NodeList tempOrg = personElement.getElementsByTagName("pageorg" + j);
                    String pageorg = tempOrg.item(0).getFirstChild().getNodeValue();
                    page.setOrgId(pageorg);
                    SceneManage.getInstance().pageBeans.add(page);
                }
            }
        }
    }

    public static ArrayList<AreaBean> parserPageArea(InputStream inputStream) throws Exception {
        ArrayList<AreaBean> areaBeanArrayList = new ArrayList<>();
        //    得到一个DocumentBuilderFactory解析工厂类
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        //    得到一个DocumentBuilder解析类
        DocumentBuilder builder = factory.newDocumentBuilder();
        //    接收一个xml的字符串来解析xml,Document代表整个xml文档
        Document document = builder.parse(inputStream);
        //    得到xml文档的根元素节点
        Element personsElement = document.getDocumentElement();
        //    得到标签为person的Node对象的集合NodeList
        NodeList nodeList = personsElement.getElementsByTagName("areas");
        for (int i = 0; i < nodeList.getLength(); i++) {
            //    如果该Node是一个Element
            if (nodeList.item(i).getNodeType() == Document.ELEMENT_NODE) {
                Element personElement = (Element) nodeList.item(i);
                NodeList childNodesList = personElement.getElementsByTagName("count");
                String count = childNodesList.item(0).getFirstChild().getNodeValue();
                int size = Integer.valueOf(count);
                for (int j = 0; j < size; j++) {
                    AreaBean areaBean = new AreaBean();
                    NodeList tempMain = personElement.getElementsByTagName("areamain" + j);
                    String pagemain = tempMain.item(0).getFirstChild().getNodeValue();
                    areaBean.setMainId(pagemain);
                    NodeList tempOrg = personElement.getElementsByTagName("areaorg" + j);
                    String pageorg = tempOrg.item(0).getFirstChild().getNodeValue();
                    areaBean.setOrgId(pageorg);
                    areaBeanArrayList.add(areaBean);
                }
            }
        }
        return areaBeanArrayList;
    }

    public static long parserTime(InputStream inputStream) throws Exception {
        ArrayList<AreaBean> areaBeanArrayList = new ArrayList<>();
        //    得到一个DocumentBuilderFactory解析工厂类
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        //    得到一个DocumentBuilder解析类
        DocumentBuilder builder = factory.newDocumentBuilder();
        //    接收一个xml的字符串来解析xml,Document代表整个xml文档
        Document document = builder.parse(inputStream);
        //    得到xml文档的根元素节点
        Element personsElement = document.getDocumentElement();
        String time = xmlNodeValue(personsElement, "pageTime");
        SimpleDateFormat ss = new SimpleDateFormat("hh:mm:ss");
        Date oldDate = ss.parse(time);
        long second = oldDate.getHours() * 3600 + oldDate.getMinutes() * 60 + oldDate.getSeconds();
        return second;
    }

    public static void parserArea(String xmlPath, AreaBean areaBean) throws Exception {
        File path = new File(Constant.DirShop,
                xmlPath + "/xml/AREA" + areaBean.getMainId());
        FileInputStream fis = new FileInputStream(path);
        //    得到一个DocumentBuilderFactory解析工厂类
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        //    得到一个DocumentBuilder解析类
        DocumentBuilder builder = factory.newDocumentBuilder();
        //    接收一个xml的字符串来解析xml,Document代表整个xml文档
        Document document = builder.parse(fis);
        //    得到xml文档的根元素节点
        Element personsElement = document.getDocumentElement();
        //    得到标签为person的Node对象的集合NodeList
        NodeList nodeList = personsElement.getElementsByTagName("area");
        for (int i = 0; i < nodeList.getLength(); i++) {
            //    如果该Node是一个Element
            if (nodeList.item(i).getNodeType() == Document.ELEMENT_NODE) {
                Element personElement = (Element) nodeList.item(i);
                float yRate = Float.valueOf(xmlNodeValue(personElement, "pageyRate"));
                float xRate = Float.valueOf(xmlNodeValue(personElement, "pagexRate"));
                areaBean.setX((int) (ScreenUtils.getScreenWidth() * xRate));
                areaBean.setY((int) (ScreenUtils.getScreenHeight() * yRate));
                float heightRate = Float.valueOf(xmlNodeValue(personElement, "heightRate"));
                float widthRate = Float.valueOf(xmlNodeValue(personElement, "widthRate"));
                areaBean.setHeight(String.valueOf((int) (ScreenUtils.getScreenHeight() * heightRate)));
                areaBean.setWidth(String.valueOf((int) (ScreenUtils.getScreenWidth() * widthRate)));
                areaBean.setMaterialType(xmlNodeValue(personElement, "materialType"));
                Log.d("holy", "jjf  = [" + i + "], areaBean = [" + areaBean.getMaterialType() + "]");
                Log.d("bbq", areaBean.getMaterialType() + " x " + areaBean.getX() + " y " + areaBean.getY() + " h " + areaBean.getHeight() + " w " + areaBean.getWidth());
                if (TextUtils.equals(areaBean.getMaterialType(), Constant.TYPE_TEXT)) {
                    areaBean.setFontSize(xmlNodeValue(personElement, "fontSize"));
                    areaBean.setBgColor(xmlNodeValue(personElement, "bgColor"));
                    areaBean.setColor(xmlNodeValue(personElement, "color"));
                    areaBean.setFontFamily(xmlNodeValue(personElement, "fontFamily"));
                    areaBean.setTextAlignx(xmlNodeValue(personElement, "textAlignx"));
                    areaBean.setTextAligny(xmlNodeValue(personElement, "textAligny"));
                    areaBean.setFontStyle(xmlNodeValue(personElement, "fontStyle"));
                    areaBean.setFilter(xmlNodeValue(personElement, "filter"));
                    areaBean.setDirection(xmlNodeValue(personElement, "direction"));
                    areaBean.setScroll(xmlNodeValue(personElement, "scroll"));
                    areaBean.setScrollSpeed(xmlNodeValue(personElement, "scrollSpeed"));
                }
                if (TextUtils.equals(areaBean.getMaterialType(), Constant.TYPE_WEATHER)) {
                    areaBean.setCityName(xmlNodeValue(personElement, "cityName"));
                }
                areaBean.setClickTo(xmlNodeValue(personElement, "clickTo"));
            }
        }
    }

    public static String xmlNodeValue(Element personElement, String tag) {
        NodeList tempOrg = personElement.getElementsByTagName(tag);
        String temp = "";
        if (tempOrg.item(0).getChildNodes().getLength() > 0) {
            temp = tempOrg.item(0).getFirstChild().getNodeValue();
        }
        return temp;
    }

    public static void parserAreaElement(String xmlPath, AreaBean areaBean) throws Exception {
        Log.d("holy", "jjf = [" + xmlPath + "], areaBean = [" + areaBean.getOrgId() + "]");
        File path = new File(Constant.DirShop,
                xmlPath + "/xml/AREA" + areaBean.getOrgId());
        FileInputStream fis = new FileInputStream(path);
        //    得到一个DocumentBuilderFactory解析工厂类
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        //    得到一个DocumentBuilder解析类
        DocumentBuilder builder = factory.newDocumentBuilder();
        //    接收一个xml的字符串来解析xml,Document代表整个xml文档
        Document document = builder.parse(fis);
        //    得到xml文档的根元素节点
        Element personsElement = document.getDocumentElement();
        //    得到标签为person的Node对象的集合NodeList
        NodeList nodeList = personsElement.getElementsByTagName("pms");
        for (int i = 0; i < nodeList.getLength(); i++) {
            //    如果该Node是一个Element
            if (nodeList.item(i).getNodeType() == Document.ELEMENT_NODE) {
                Element personElement = (Element) nodeList.item(i);
                NodeList childNodesList = personElement.getElementsByTagName("count");
                if (childNodesList.item(0) == null) {
                    break;
                }
                String count = childNodesList.item(0).getFirstChild().getNodeValue();
                int size = Integer.valueOf(count);
                for (int j = 0; j < size; j++) {
                    NodeList tempMain = personElement.getElementsByTagName("pmmain" + j);
                    String pagemain = tempMain.item(0).getFirstChild().getNodeValue();
                    areaBean.elements.add(pagemain);
                }
            }
        }
    }

    public static void parserElement(String xmlPath, AreaBean areaBean) throws Exception {
        for (int i = 0; i < areaBean.elements.size(); i++) {
            String id = areaBean.elements.get(i);
            File path = new File(Constant.DirShop,
                    xmlPath + "/xml/MATE" + id);
            FileInputStream fis = new FileInputStream(path);
            //    得到一个DocumentBuilderFactory解析工厂类
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            //    得到一个DocumentBuilder解析类
            DocumentBuilder builder = factory.newDocumentBuilder();
            //    接收一个xml的字符串来解析xml,Document代表整个xml文档
            Document document = builder.parse(fis);
            //    得到xml文档的根元素节点
            Element personsElement = document.getDocumentElement();
            //    得到标签为person的Node对象的集合NodeList
            NodeList nodeList = personsElement.getElementsByTagName("area");
            for (int j = 0; j < nodeList.getLength(); j++) {
                //    如果该Node是一个Element
                if (nodeList.item(j).getNodeType() == Document.ELEMENT_NODE) {
                    Element personElement = (Element) nodeList.item(j);
                    if (TextUtils.equals(areaBean.getMaterialType(), Constant.TYPE_TEXT) || TextUtils.equals(areaBean.getMaterialType(), Constant.TYPE_WEB)) {
                        String txtContent = xmlNodeValue(personElement, "txtContent");
                        areaBean.setTxtContent(txtContent);
                        continue;
                    }
                    if (TextUtils.equals(areaBean.getMaterialType(), Constant.TYPE_STREAM)) {
                        String txtContent = xmlNodeValue(personElement, "txtContent");
                        areaBean.elementUrls.add(txtContent);
                        continue;
                    }
                    String url = xmlNodeValue(personElement, "url");
                    url = Constant.DirShop + url;
                    if (TextUtils.equals(areaBean.getMaterialType(), Constant.TYPE_EXCEL) || TextUtils.equals(areaBean.getMaterialType(), Constant.TYPE_DOC)
                            || TextUtils.equals(areaBean.getMaterialType(), Constant.TYPE_PDF)) {
                        ArrayList<File> files = new ArrayList<>();
                        files.addAll(FileUtils.listFilesInDir(url));
                        areaBean.setTime(2);
                        for (int k = 0; k < files.size(); k++) {
                            areaBean.elementUrls.add(files.get(k).getAbsolutePath());
                        }
                        continue;
                    }
                    if (TextUtils.equals(areaBean.getMaterialType(), Constant.TYPE_PPT)) {
                        areaBean.elementUrls.add(url);
                        continue;
                    }
                    Log.d("holy", "DomParserUtils parserElement() called with: " + "areaBean = [" + url + "]");
                    areaBean.elementUrls.add(url);
                    String elementTime = xmlNodeValue(personElement, "pmTime");
                    areaBean.elementTimes.add(TimeUtils.millFromString(elementTime));


                    if (TextUtils.equals(areaBean.getMaterialType(), Constant.TYPE_MUT)) {
                        String pmTime = xmlNodeValue(personElement, "pmTime");
                        long time = TimeUtils.millFromString(pmTime);
                        String type = xmlNodeValue(personElement, "materialType");
                        if (TextUtils.equals(type, Constant.TYPE_TEXT)) {
                            TextElement textElement = new TextElement();
                            String txtContent = xmlNodeValue(personElement, "txtContent");
                            textElement.txtContent = txtContent;
                            textElement.time = time;
                            areaBean.baseElements.add(textElement);
                        }
                        if (TextUtils.equals(type, Constant.TYPE_WEB)) {
                            WebElement webElement = new WebElement();
                            String txtContent = xmlNodeValue(personElement, "txtContent");
                            webElement.txtContent = txtContent;
                            webElement.time = time;
                            areaBean.baseElements.add(webElement);
                        }
                        if (TextUtils.equals(type, Constant.TYPE_VIDEO)) {
                            VideoElement videoElement = new VideoElement();
                            videoElement.txtContent = url;
                            videoElement.time = time;
                            areaBean.baseElements.add(videoElement);
                        }
                        if (TextUtils.equals(type, Constant.TYPE_STREAM)) {
                            VideoElement videoElement = new VideoElement();
                            String txtContent = xmlNodeValue(personElement, "txtContent");
                            videoElement.txtContent = txtContent;
                            videoElement.time = time;
                            areaBean.baseElements.add(videoElement);
                        }
                        if (TextUtils.equals(type, Constant.TYPE_BARCODE)) {
                            ImageElement imageElement = new ImageElement();
                            imageElement.txtContent = url;
                            imageElement.time = time;
                            areaBean.baseElements.add(imageElement);
                        }
                        if (TextUtils.equals(type, Constant.TYPE_IMG)) {
                            if (FileUtils.isFile(url)) {
                                ImageElement imageElement = new ImageElement();
                                imageElement.txtContent = url;
                                imageElement.time = time;
                                areaBean.baseElements.add(imageElement);
                            } else if (FileUtils.isDir(url)) {
                                ViewPagerElement viewPagerElement = new ViewPagerElement();
                                ArrayList<File> files = new ArrayList<>();
                                List<File> ff = FileUtils.listFilesInDir(url);
                                files.addAll(ff);
                                for (int k = 0; k < files.size(); k++) {
                                    viewPagerElement.imgUrls.add(files.get(k).getAbsolutePath());
                                }
                                viewPagerElement.time = time;
                                areaBean.baseElements.add(viewPagerElement);
                            }
                        }
                        if (TextUtils.equals(type, Constant.TYPE_EXCEL) || TextUtils.equals(type, Constant.TYPE_DOC)
                                || TextUtils.equals(type, Constant.TYPE_PPT) || TextUtils.equals(type, Constant.TYPE_PDF)) {
                            ViewPagerElement viewPagerElement = new ViewPagerElement();
                            ArrayList<File> files = new ArrayList<>();
                            List<File> ff = FileUtils.listFilesInDir(url);
                            files.addAll(ff);
                            for (int k = 0; k < files.size(); k++) {
                                viewPagerElement.imgUrls.add(files.get(k).getAbsolutePath());
                            }
                            viewPagerElement.time = time;
                            areaBean.baseElements.add(viewPagerElement);
                        }

                        continue;
                    }
                }
            }
        }
    }
}