package com.tiramisu.show;




/**
 * Created by 王新 on 2017-3-27.
 */
public class Cache {
    private String key;
    private String value;
    private String nowBig;
    private String big;
    public Cache() {
        super();
    }

    public Cache(String key, String value,String nowBig,String big) {
        this.key = key;
        this.value = value;
        this.nowBig=nowBig;
        this.big=big;
    }

    public String getNowBig() {
        return nowBig;
    }

    public void setNowBig(String nowBig) {
        this.nowBig = nowBig;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public String getBig() {
        return big;
    }

    public void setBig(String big) {
        this.big = big;
    }

    public void setKey(String string) {
        key = string;
    }






}



