package com.tiramisu.show;

/**
 * Created by 16010046 on 2017/4/20.
 */
public class Person {
    private int id;
    private String name;
    private int age;

    public void setId(int i) {
        id = i;
    }

    public void setName(String n) {
        name = n;
    }

    public void setAge(int i) {
        age = i;
    }
}
