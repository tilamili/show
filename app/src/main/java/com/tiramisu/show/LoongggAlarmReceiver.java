package com.tiramisu.show;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;

import com.tiramisu.show.bean.EventBean;
import com.tiramisu.show.util.AlarmManagerUtil;
import com.tiramisu.show.util.FileLog;
import com.tiramisu.show.util.TimeUtils;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.EventBus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.tiramisu.show.util.TimeUtils.getNowString;

/**
 * Created by loongggdroid on 2016/3/21.
 */
public class LoongggAlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        int type = intent.getIntExtra("type", 0);
        FileLog.d("holy", "收到广播" + type);
        if (type == 0) {
            String now = getNowString(new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()));
            String week = TimeUtils.getWeekIndex(new Date()) + "";
            String json = FileUtils.readFile2String("/sdcard/plan.xml", "UTF-8");
            if (TextUtils.isEmpty(json)) {
                return;
            }
            int index = 0;
            json = json.replace("'", "\"");
            if (!TextUtils.isEmpty(json)) {
                List<PlanBean> planBeans = PlanBean.parsePlan(json);
                for (int i = 0; i < planBeans.size(); i++) {
                    PlanBean p = planBeans.get(i);
                    String n = p.getProWeek();
                    String key = "";
                    if (TextUtils.equals(now, n) || TextUtils.equals(week, n)) {
                        key = n;
                    }
                    if (!TextUtils.isEmpty(key)) {
                        //开始闹钟
                        SimpleDateFormat sim1 = new SimpleDateFormat("HH:mm:ss");
                        try {
                            Date date1 = sim1.parse(sim1.format(new Date()));
                            if ((date1.before(sim1.parse(p.getEndTime())) && date1.after(sim1.parse(p.getBeginTime())))) { //不是白班
                                EventBean eventBean = new EventBean();
                                eventBean.setId(p.getContentId());
                                EventBus.getDefault().post(new MessageEvent(EventConstant.EventPlay, eventBean));
                                FileLog.d("holy", "直接播放");
                            }
                            String[] startTime = p.getBeginTime().split(":");
                            FileLog.d("holy", "设置时间" + p.getBeginTime() + "-" + p.getEndTime());
                            FileLog.d("holy", "播放目录" + p.getContentId());
                            MobclickAgent.onEvent(context, "begin", p.getBeginTime());
                            AlarmManagerUtil.setAlarm(context, 0, Integer.valueOf(startTime[0]), Integer.valueOf(startTime[1]), index++, 0, 1, p.getContentId());
                            String[] endTime = p.getEndTime().split(":");
//                    String[] endTime = "18:55:00".split(":");
                            MobclickAgent.onEvent(context, "end", p.getEndTime());
                            AlarmManagerUtil.setAlarm(context, 0, Integer.valueOf(endTime[0]), Integer.valueOf(endTime[1]), index++, 0, 2, p.getContentId());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else if (type == 1) {
            MobclickAgent.onEvent(context, "start", TimeUtils.getNowString());
            FileLog.d("holy", "开始播放" + intent.getStringExtra("contentId"));
            EventBean eventBean = new EventBean();
            eventBean.setId(intent.getStringExtra("contentId"));
            EventBus.getDefault().post(new MessageEvent(EventConstant.EventPlay, eventBean));
        } else if (type == 2) {
            MobclickAgent.onEvent(context, "end", TimeUtils.getNowString());
            FileLog.d("holy", "结束播放" + intent.getStringExtra("contentId"));
            EventBus.getDefault().post(new MessageEvent(EventConstant.EventClose));
        }

//        EventBean eventBean = new EventBean();
//        eventBean.setId("c1224040f8bf4474ba8ceeb2b3aaeeca");
//        EventBus.getDefault().post(new MessageEvent(EventConstant.EventPlay, eventBean));
//        long intervalMillis = intent.getLongExtra("intervalMillis", 0);
//        if (intervalMillis != 0) {
//            AlarmManagerUtil.setAlarmTime(context, System.currentTimeMillis() + intervalMillis,
//                    intent);
//        }
//        int flag = intent.getIntExtra("soundOrVibrator", 0);
//        Intent clockIntent = new Intent(context, ClockAlarmActivity.class);
//        clockIntent.putExtra("msg", msg);
//        clockIntent.putExtra("flag", flag);
//        clockIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        context.startActivity(clockIntent);
    }


}
