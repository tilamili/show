package com.tiramisu.show;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.tiramisu.show.element.BaseElement;
import com.tiramisu.show.element.ImageElement;
import com.tiramisu.show.element.TextElement;
import com.tiramisu.show.element.VideoElement;
import com.tiramisu.show.element.ViewPagerElement;
import com.tiramisu.show.element.WebElement;
import com.tiramisu.show.util.ViewUtil;

import java.util.ArrayList;

/**
 * Created by 16010046 on 2017/4/21.
 */

public class MutArea extends AreaBean {

    private ArrayList<BaseElement> mElements = new ArrayList<>();
    private BaseElement mCurrentElement;
    private int mIndex;
    private View mCurrentView;
    private RelativeLayout mParentView;
    private int mElementIndex = 0;
    private Activity mActivity;
    private AreaBean mAreaBean;

    public void build(int index, RelativeLayout root, AreaBean a, final Activity activity) {
        mActivity = activity;
        mAreaBean = a;
        mParentView = root;
        mIndex = index;
        mElements = a.baseElements;
        mCurrentElement = a.baseElements.get(0);
        makeView(root, a, activity, mCurrentElement);
    }

    private void makeView(RelativeLayout root, AreaBean a, final Activity activity, BaseElement baseElement) {
        mCurrentElement = baseElement;
        if (mCurrentElement instanceof ViewPagerElement) {
            ViewPagerElement viewPagerElement = (ViewPagerElement) mCurrentElement;
            mCurrentView = ViewUtil.buildViewPager(root, a, activity, viewPagerElement.imgUrls);
        } else if (mCurrentElement instanceof VideoElement) {
            VideoElement videoElement = (VideoElement) mCurrentElement;
            mCurrentView = ViewUtil.buildVideoView(root, a, activity, videoElement.txtContent);
        } else if (mCurrentElement instanceof TextElement) {
            TextElement textElement = (TextElement) mCurrentElement;
            mCurrentView = ViewUtil.buildTextView(root, a, activity, textElement.txtContent);
        } else if (mCurrentElement instanceof WebElement) {
            WebElement webElement = (WebElement) mCurrentElement;
            mCurrentView = ViewUtil.buildWebView(root, a, activity, webElement.txtContent);
        } else if (mCurrentElement instanceof ImageElement) {
            ImageElement imageElement = (ImageElement) mCurrentElement;
            mCurrentView = ViewUtil.buildImageView(root, a, activity, imageElement.txtContent);
        }
    }

    @Override
    public void handleMessage(Handler handler, Message message) {
        if (message.arg1 == 0) {
            mParentView.removeView(mCurrentView);
            mElementIndex++;
            makeView(mParentView, mAreaBean, mActivity, mElements.get(mElementIndex % mElements.size()));
            Message msg = new Message();
            msg.what = mIndex;
            msg.arg1 = 0;
            handler.sendMessageDelayed(msg, mCurrentElement.time * 1000);
            if (mCurrentElement instanceof ViewPagerElement) {
                ViewPager viewPager = (ViewPager) mCurrentView;
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                Message temp = new Message();
                temp.what = mIndex;
                temp.arg1 = 1;
                handler.sendMessageDelayed(temp, 2000);
            }
        } else {
            if (mCurrentElement instanceof ViewPagerElement) {
                ViewPager viewPager = (ViewPager) mCurrentView;
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                Message msg = new Message();
                msg.what = mIndex;
                msg.arg1 = 1;
                handler.sendMessageDelayed(msg, 2000);
            }
        }
    }
}
