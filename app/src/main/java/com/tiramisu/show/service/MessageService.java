package com.tiramisu.show.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.tiramisu.show.LoginRun;

import java.util.HashMap;
import java.util.Map;

public class MessageService extends Service {

    public static final String TAG = "MessageService";

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            new Thread(new LoginRun(map)).start();
            mHandler.sendEmptyMessageDelayed(0, 60 * 1000);
        }
    };

    Map map;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_STICKY;
        }
        Log.d("show123", "onStartCommand() called with: intent = [" + intent + "], flags = [" + flags + "], startId = [" + startId + "]");
        if (intent.hasExtra("stop")) {
            mHandler.removeCallbacksAndMessages(null);
            Log.d("holy", "MessageService onStartCommand() called with: " + "intent = [" + intent + "], flags = [" + flags + "], startId = [" + startId + "]");
            stopSelf();
        }
        String ip = intent.getStringExtra("ip");
        String name = intent.getStringExtra("name");
        String pwd = intent.getStringExtra("pwd");
        map = new HashMap();
        map.put("IP", ip);
        map.put("shortName", name);
        map.put("password", pwd);
        map.put("port", "8899");
        mHandler.sendEmptyMessage(0);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d("holy", "MessageService onDestroy() called with: " + "");
        super.onDestroy();
    }
}