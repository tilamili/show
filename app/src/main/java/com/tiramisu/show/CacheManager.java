package com.tiramisu.show;


import java.util.Date;
import java.util.HashMap;

/**
 * Created by 王新 on 2017-3-27.
 */
public class CacheManager {
    private static HashMap cacheMap = new HashMap();


    /**
     * This class is singleton so private constructor is used.
     */
    public CacheManager() {
        super();
    }


    /**
     * returns cache item from hashmap
     * @param key
     * @return Cache
     */
    private synchronized static Cache getCache(String key) {
        return (Cache)cacheMap.get(key);
    }


    /**
     * Looks at the hashmap if a cache item exists or not
     * @param key
     * @return Cache
     */
    private synchronized static boolean hasCache(String key) {
        return cacheMap.containsKey(key);
    }


    /**
     * Invalidates all cache
     */
    public synchronized static void invalidateAll() {
        cacheMap.clear();
    }


    /**
     * Invalidates a single cache item
     * @param key
     */
    public synchronized static void invalidate(String key) {
        cacheMap.remove(key);
    }


    /**
     * Adds new item to cache hashmap
     * @param key
     * @return Cache
     */
    public synchronized static void putCache(String key, Cache cache) {
        cacheMap.put(key, cache);
    }


    /**
     * Reads a cache item's content
     * @param key
     * @return
     */
    public static Cache getContent(String key) {
        if (hasCache(key)) {
            Cache cache = getCache(key);
            return cache;
        } else {
            return null;
        }
    }


    /**
     *
     * @param key
     * @param content
     */
    public static void putContent(String keypk,String key, String content,String big,String nowBig) {
        invalidate(keypk);
        Cache cache = new Cache();
        cache.setKey(key);
        cache.setValue(content);
        cache.setBig(big);
        cache.setNowBig(nowBig);
        putCache(keypk, cache);
    }




}



