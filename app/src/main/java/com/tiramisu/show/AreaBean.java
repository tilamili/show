package com.tiramisu.show;

import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.tiramisu.show.element.BaseElement;
import com.tiramisu.show.media.IjkVideoView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;


public class AreaBean {
    private String mainId;
    private String orgId;
    private Double height;
    private Double width;
    private ViewPager mViewPager;
    private String materialType;
    private String txtContent;
    protected String clickTo;
    private int mTime = 10000;

    public String getFontSize() {
        return mFontSize;
    }

    public String getBgColor() {
        return mBgColor;
    }

    public String getColor() {
        return mColor;
    }

    public String getFontFamily() {
        return mFontFamily;
    }

    public String getTextAlignx() {
        return mTextAlignx;
    }

    public String getTextAligny() {
        return mTextAligny;
    }

    public String getFontStyle() {
        return mFontStyle;
    }

    public String getFilter() {
        return mFilter;
    }

    public String getDirection() {
        return mDirection;
    }

    public String getScroll() {
        return mScroll;
    }

    public String getScrollSpeed() {
        return mScrollSpeed;
    }

    private String mFontSize;
    private String mBgColor;
    private String mColor;
    private String mFontFamily;
    private String mTextAlignx;
    private String mTextAligny;
    private String mFontStyle;
    private String mFilter;
    private String mDirection;
    private String mScroll;
    private String mScrollSpeed;

    public String getCityName() {
        return mCityName;
    }

    public void setCityName(String cityName) {
        this.mCityName = cityName;
    }

    private String mCityName;

    public String getClickTo() {
        return clickTo;
    }

    public void setClickTo(String clickTo) {
        this.clickTo = clickTo;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    private int x;
    private int y;
    public ArrayList<String> elements = new ArrayList<>();
    public ArrayList<BaseElement> baseElements = new ArrayList<>();
    public ArrayList<String> elementUrls = new ArrayList<>();
    public ArrayList<Integer> elementTimes = new ArrayList<>();

    public String getTxtContent() {
        return txtContent;
    }

    public void setTxtContent(String txtContent) {
        this.txtContent = txtContent;
    }

    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    public Double getWidth() {
        return width;
    }

    public Double getHeight() {
        return height;
    }

    public String getMainId() {
        return mainId;
    }

    public void setMainId(String mainId) {
        this.mainId = mainId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public void setHeight(String h) {
        height = Double.valueOf(h);
    }

    public void setWidth(String w) {
        width = Double.valueOf(w);
    }

    public void setViewPage(ViewPager v) {
        mViewPager = v;
    }

    public void handleMessage() {
        if (TextUtils.equals(materialType, Constant.TYPE_IMG) || TextUtils.equals(materialType, Constant.TYPE_PDF)) {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
        }
    }

    public void handleMessage(Handler handler, Message message) {
//        if (TextUtils.equals(materialType, Constant.TYPE_IMG) || TextUtils.equals(materialType, Constant.TYPE_EXCEL) || TextUtils.equals(materialType, Constant.TYPE_DOC) ||
//                TextUtils.equals(materialType, Constant.TYPE_PDF)) {
//            if (mViewPager != null) {
//                mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
//            }
//        }
    }

    public int getTime() {
        return mTime;
    }

    public void setVideoView(IjkVideoView videoView) {
    }

    public void setTextView(TextView textView) {
    }

    public void setClick(View view, final String id) {
        if (!TextUtils.isEmpty(id)) {
            if (view instanceof WebView) {
                WebView webView = (WebView) view;
                webView.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                            EventBus.getDefault().post(new MessageEvent(EventConstant.EventClick, id));
                        }
                        Log.d("holy", "onTouch() called with: view = [" + view + "], motionEvent = [" + motionEvent + "]");
                        return false;
                    }
                });
            } else {
                view.setOnClickListener(new android.view.View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        EventBus.getDefault().post(new MessageEvent(EventConstant.EventClick, id));
                    }
                });
            }
        }
    }

    public void setTime(int i) {
        mTime = i * 1000;
    }

    public void setFontSize(String fontSize) {
        mFontSize = fontSize;
    }

    public void setBgColor(String bgColor) {
        mBgColor = bgColor;
    }

    public void setColor(String color) {
        mColor = color;
    }

    public void setFontFamily(String fontFamily) {
        mFontFamily = fontFamily;
    }

    public void setTextAlignx(String textAlignx) {
        mTextAlignx = textAlignx;
    }

    public void setTextAligny(String textAligny) {
        mTextAligny = textAligny;
    }

    public void setFontStyle(String fontStyle) {
        mFontStyle = fontStyle;
    }

    public void setFilter(String filter) {
        mFilter = filter;
    }

    public void setDirection(String direction) {
        mDirection = direction;
    }

    public void setScroll(String scroll) {
        mScroll = scroll;
    }

    public void setScrollSpeed(String scrollSpeed) {
        mScrollSpeed = scrollSpeed;
    }
}
