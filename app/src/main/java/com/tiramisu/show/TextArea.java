package com.tiramisu.show;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tiramisu.show.util.IOUtils;
import com.tiramisu.show.util.ViewUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by 16010046 on 2017/4/21.
 */

public class TextArea extends AreaBean {

    public void build(RelativeLayout root, AreaBean a, final Activity activity, String txtContent) {

        //插播文字
        final WebView webview = new WebView(activity);
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);  //支持js
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setSupportMultipleWindows(true);
        webview.setWebViewClient(new WebViewClient());
        webview.setWebChromeClient(new WebChromeClient());
        try {
//            String content = IOUtils.toString(activity.getAssets().open("error.html"))
//                    .replaceAll("%ERR_TITLE%", "bbq")
//                    .replaceAll("%ERR_DESC%", "ddr");
//
//            webview.loadDataWithBaseURL("file:///android_asset/error.html", content, "text/html", "UTF-8", null);

//                    "sefwefefsf_FFFFFF_000000_PMingLiU_36_0_0__100_0_2_30_200_200"

            String bs = txtContent + "_" + a.getBgColor() + "_" + a.getColor() + "_" + a.getFontFamily() + "_" + a.getFontSize() +
                    "_" + a.getTextAlignx() + "_" + a.getTextAligny() + "_" + a.getFontStyle() + "_" + a.getFilter() + "_" + a.getDirection() + "_" + a.getScroll() +
                    "_" + a.getScrollSpeed() + "_" + a.getWidth().intValue() + "_" + a.getHeight().intValue();
            String content = IOUtils.toString(activity.getAssets().open("help.html"))
                    .replaceAll("%ERR_TITLE%", bs)
                    .replaceAll("%ERR_DESC%", "ddr");

            webview.loadDataWithBaseURL("file:///android_asset/help.html", content, "text/html", "UTF-8", null);
            ViewUtil.addView(root, webview, a);
            setClick(webview, a.clickTo);
        } catch (Exception e) {
        }
    }
}
