package com.tiramisu.show;

import android.app.Application;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.tiramisu.show.util.NetworkUtils;
import com.tiramisu.show.util.TimeUtils;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import tv.danmaku.ijk.media.player.IjkMediaPlayer;

import static com.tiramisu.show.Constant.DirShop;

/**
 * Created by 16010046 on 2017/5/15.
 */

public class ShowApplication extends Application {
    public static Application mApplication;
    public static boolean isOutOfDate = false;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("holy1", "程序启动");
        mApplication = this;
        IjkMediaPlayer.loadLibrariesOnce(null);
        IjkMediaPlayer.native_profileBegin("libijkplayer.so");
//        AECrashHelper.initCrashHandler(this, new AECHConfiguration.Builder()
//                .setSaveToLocal(true)
//                .setReportToServer(true)
//                .setReporter(new AECHConfiguration.IAECHReporter() {
//                    @Override
//                    public void report(Throwable ex) {
//                    }
//                })
////              .setLocalFolderPath()//设置本地异常日志文件存储路径
//                .build());
//        isOutOfDate = !getPassNew();
//        isOutOfDate = false;
        Cache cc = new Cache();
        cc.setKey("");
        cc.setValue("0");
        cc.setBig("0");
        cc.setNowBig("0");
        CacheManager.putCache("jindu", cc);
        FileUtils.createOrExistsDir(DirShop);
    }

    public static boolean getPassNew() {
        boolean isOk = false;
        String keyPath = "/sdcard/benzi.bz";
        File keyf = new File(keyPath);
        if (keyf.exists()) {
            try {
                String datestr = goPassUtil.readTxtFile(keyf);
                String date = goPassUtil.getPass(datestr.split("null")[1]).split("_")[0];
                String mac = goPassUtil.getPass(datestr.split("null")[1]).split("_")[1];
                String macn = NetworkUtils.getMacAddress(mApplication).replace(':', '-');
                Log.d("holy", mac + " " + macn);
                if (TextUtils.equals(macn, mac) && compareDate(date, TimeUtils.date2String(new Date(), new SimpleDateFormat("yyyy-MM-dd")))) {
                    isOk = true;
                } else {
                    if (!TextUtils.equals(macn, mac)) {
                        Toast.makeText(mApplication, "MAC地址不匹配" + macn + "-" + mac, Toast.LENGTH_LONG).show();
                    }
                    if (!compareDate(date, TimeUtils.date2String(new Date(), new SimpleDateFormat("yyyy-MM-dd")))){
                        Toast.makeText(mApplication, "日期不匹配" + date + "-" + TimeUtils.date2String(new Date(), new SimpleDateFormat("yyyy-MM-dd")), Toast.LENGTH_LONG).show();
                    }
                    isOk = false;
                }
            } catch (Exception e) {
                isOk = false;
                e.printStackTrace();
            }
        } else {
            isOk = false;
        }
        return isOk;
    }

    public static boolean getPass() {
        boolean isOk = false;
        String keyPath = "/sdcard/benzi.bz";
        File keyf = new File(keyPath);
        if (keyf.exists()) {
            try {
                String datestr = goPassUtil.readTxtFile(keyf);
                String date = goPassUtil.getPass(datestr.split("null")[1]);
                System.out.println(date);
                if (compareDate(date, TimeUtils.date2String(new Date(), new SimpleDateFormat("yyyy-MM-dd")))) {
                    isOk = true;
                } else {
                    isOk = false;
                }
            } catch (Exception e) {
                isOk = false;
                e.printStackTrace();
            }
        } else {
            isOk = false;
        }
        return isOk;
    }

    public static boolean compareDate(String s, String e) {
        if (fomatDate(s) == null || fomatDate(e) == null) {
            return false;
        }
        return fomatDate(s).getTime() >= fomatDate(e).getTime();
    }

    public static Date fomatDate(String date) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (date.equals(""))
                return null;
            else
                return fmt.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.d("holy1", "程序关闭");
    }
}
