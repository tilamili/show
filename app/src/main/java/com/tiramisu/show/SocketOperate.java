package com.tiramisu.show;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;

import com.tiramisu.show.bean.EventBean;
import com.tiramisu.show.util.SPUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class SocketOperate extends Thread {
    private Socket socket;

    public SocketOperate(Socket socket) {
        this.socket = socket;
    }


    @SuppressWarnings("unused")
    public void run() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
            StringBuilder sb = new StringBuilder();
            String temp;
            int index;
            while ((temp = br.readLine()) != null) {
                System.out.println(temp);
                if ((index = temp.indexOf("ends")) != -1) {//遇到eof时就结束接收
                    sb.append(temp.substring(0, index));
                    break;
                }
                sb.append(temp);
            }
            Log.d("show123", sb.toString());
            if (sb.toString().split(";")[0].equals("1")) {
                // TODO: 17/5/14 截图
                try {
//                        Writer writer = null;
//                        writer = new OutputStreamWriter(socket.getOutputStream(),"UTF-8");
//                        writer.write("4;");
//                        writer.write("ends\n");
//                        writer.flush();

                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    DataOutputStream dout = new DataOutputStream(socket.getOutputStream());
//                        int screenWidth = ((int) java.awt.Toolkit.getDefaultToolkit().getScreenSize().width);
//                        int screenHeight = ((int) java.awt.Toolkit.getDefaultToolkit().getScreenSize().height);
//                        BufferedImage bi = RuYunUtil.getScreenShot(0, 0, screenWidth, screenHeight);
//                        ImageIO.write(bi, "jpg", out);
                    EventBus.getDefault().post(new MessageEvent(EventConstant.EventCap));
                    while (!FileUtils.isFileExists("/sdcard/cap.jpg")) {

                    }
//                        Bitmap bitmap = MainActivity.getBitmap();
                    byte[] b = SocketOperate.File2byte("/sdcard/cap.jpg");

                    dout.write(b);
                    dout.flush();
                    dout.close();
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    FileUtils.deleteDir("/sdcard/cap.jpg");
                }
            }
            if (sb.toString().split(";")[0].equals("2")) {
                Log.d("show123", "run() called");
                // TODO: 17/5/14 插播图片
                String imgName = sb.toString().split(";")[1];
                String width = sb.toString().split(";")[2];
                String height = sb.toString().split(";")[3];
                String pageX = sb.toString().split(";")[4];
                String pageY = sb.toString().split(";")[5];
                String times = sb.toString().split(";")[6];
                Writer writer = null;
                writer = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
                writer.write("okImg;");
                writer.write("ends\n");
                writer.flush();


                DataInputStream inputStream = null;
                try {
                    inputStream = getMessageStream();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String basePath = "/sdcard/img/";
                FileUtils.createOrExistsDir(basePath);
                int bufferSize = 8192;
                byte[] buf = new byte[bufferSize];
                int passedlen = 0;
                long len = 0;
//                    FileUtils.writeFileFromIS(basePath + imgName, inputStream, false);
                String savePath = basePath + inputStream.readUTF();
                System.out.println("文件名:" + savePath);
                DataOutputStream fileOut = new DataOutputStream(new FileOutputStream(savePath));
                System.out.println("文件名:" + savePath);
                len = inputStream.readLong();

                System.out.println("文件的长度为:" + len + "\n");
                System.out.println("开始接收文件!" + "\n");

                while (true) {
                    int read = 0;
                    if (inputStream != null) {
                        read = inputStream.read(buf);
                    }
                    passedlen += read;
                    if (read == -1) {
                        break;
                    }
                    //下面进度条本为图形界面的prograssBar做的，这里如果是打文件，可能会重复打印出一些相同的百分比
                    System.out.println("文件接收了" + (passedlen * 100 / len) + "%\n");
                    fileOut.write(buf, 0, read);
                }
                EventBean eventBean = new EventBean();
                eventBean.setHeight(height);
                eventBean.setWidth(width);
                eventBean.setPageX(pageX);
                eventBean.setPageY(pageY);
                eventBean.setTimes(times);
                eventBean.setTextStyle(savePath);
                eventBean.setType(1);
                EventBus.getDefault().post(new MessageEvent(EventConstant.EventText, eventBean));
                System.out.println("接收完成，文件存为" + savePath + "\n");
//                    Platform.runLater(() -> {
//                        GoInsert gi = new GoInsert();
//                        gi.getInsertStage(savePath, Integer.parseInt(width), Integer.parseInt(height), Integer.parseInt(pageX), Integer.parseInt(pageY), Integer.parseInt(times));
//                    });
                fileOut.close();
                socket.close();
            }
            if (sb.toString().split(";")[0].equals("3")) {

                // TODO: 17/5/14 插播文字
                String width = sb.toString().split(";")[1];
                String height = sb.toString().split(";")[2];
                String pageX = sb.toString().split(";")[3];
                String pageY = sb.toString().split(";")[4];
                String times = sb.toString().split(";")[5];
                String textStyle = sb.toString().split(";")[6];
                EventBean eventBean = new EventBean();
                eventBean.setHeight(height);
                eventBean.setWidth(width);
                eventBean.setPageX(pageX);
                eventBean.setPageY(pageY);
                eventBean.setTimes(times);
                eventBean.setTextStyle(textStyle);
                eventBean.setType(2);
                EventBus.getDefault().post(new MessageEvent(EventConstant.EventText, eventBean));
                //3;100;100;100;100;60;sefwefefsf_FFFFFF_000000_PMingLiU_36_0_0__100_0_2_30
//                    Platform.runLater(() -> {
//                        GoInsert gi = new GoInsert();
//                        gi.getInsertTextStage(Integer.parseInt(width), Integer.parseInt(height), Integer.parseInt(pageX), Integer.parseInt(pageY), Integer.parseInt(times), textStyle);
//                    });
                socket.close();
            }
            if(sb.toString().split(";")[0].equals("4")){
                String str=sb.toString().split(";")[1];
                if(str.equals("cleanPc")){
                    // TODO: 18/4/18 清理目录 
                }
                if(str.equals("stopFx")){
                    // TODO: 18/4/18 暂停播放 
//                    Platform.runLater(() -> {
//                        String nowPlanPath = "D:\\javafx\\plan\\nowPlan.xml";
//                        File npfilr = new File(nowPlanPath);
//                        if (npfilr.exists()) {
//                            npfilr.delete();
//                        }
//                        Main main = new Main();
//                        main.closeStage();
//                    });
                }
                if(str.equals("startFx")){
                    // TODO: 18/4/18 开始播放 
//                    Platform.runLater(() -> {
//                        Main main = new Main();
////                            Timer timer = new Timer();
////                            main.updateStage(timer);
//                        main.restartMe();
//                    });
                }
                socket.close();
            }
            if (sb.toString().split(";")[0].equals("6")) {
                Cache cc = CacheManager.getContent("jindu");
                Writer writer = null;
                writer = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
                writer.write(cc.getValue() + ";" + cc.getBig() + ";" + cc.getNowBig());
                writer.write("ends\n");
                writer.flush();
                socket.close();
            }

            if (sb.toString().split(";")[0].equals("7")) {
                Log.d("qiufeng", "文件接收");
                String proStr = sb.toString().split(";")[1];
                String planStr = sb.toString().split(";")[2];
                if (!TextUtils.isEmpty(proStr) && proStr.split("_").length > 0) {
                    for (String proId : proStr.split("_")) {
                        if (!TextUtils.isEmpty(proId)) {
//                                so.getSocketLog(map,"1;"+proId);
                            Writer writer = null;
                            writer = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
                            writer.write("getPro;" + proId);
                            writer.write("ends\n");
                            writer.flush();

                            DataInputStream inputStream = null;
                            try {
                                inputStream = getMessageStream();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            String basePath = Constant.DirShop + "/";
                            int bufferSize = 8192;
                            byte[] buf = new byte[bufferSize];
                            int passedlen = 0;
                            long len = 0;
                            String fileName = inputStream.readUTF();
                            String savePath = basePath + fileName;
                            DataOutputStream fileOut = new DataOutputStream(new BufferedOutputStream(new BufferedOutputStream(new FileOutputStream(savePath))));
                            len = inputStream.readLong();

                            while (true) {
                                int read = 0;
                                if (inputStream != null) {
                                    read = inputStream.read(buf);
                                }
                                passedlen += read;
                                if (read == -1) {
                                    break;
                                }
                                CacheManager.putContent("jindu", fileName, String.valueOf((passedlen * 100 / len)), String.valueOf(len), String.valueOf(passedlen));
                                fileOut.write(buf, 0, read);
                            }
                            writer.close();
                            fileOut.close();
                            socket.close();
                            String path = basePath + proId;
                            new SPUtils(ShowApplication.mApplication, "show").put("id", fileName.replace(".zip", ""));
                            ZipUtils.unzipFile(savePath, path);
                            File f = new File(savePath);
                            if (f.exists()) {
                                f.delete();
                            }
                        }
                    }

                }

                if (!TextUtils.isEmpty(planStr)) {
                    String planPath = "/sdcard/plan.xml";
                    Map m = new HashMap();
                    m.put("planContext", planStr);
                    FileUtils.deleteFile(planPath);
                    FileUtils.writeFileFromString(planPath, planStr, false);
                }
                EventBus.getDefault().post(new MessageEvent(EventConstant.EventPlan));
            }
            if (sb.toString().split(";")[0].equals("8")) {
                Writer writer = null;
                writer = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
                writer.write("ok");
                writer.write("ends\n");
                writer.flush();
                writer.close();
                socket.close();
            }
            if (sb.toString().split(";")[0].equals("9")) {
                System.out.println(sb.toString() + "+++++++tos");
                String proStr = sb.toString().split(";")[1];
                final String planStr = sb.toString().split(";")[2];
                String oldLength = sb.toString().split(";")[3];
                if (TextUtils.isEmpty(proStr) && proStr.split("_").length > 0) {

                    for (String proId : proStr.split("_")) {
                        if (TextUtils.isEmpty(proId)) {
//                                so.getSocketLog(map,"1;"+proId);
                            Writer writer = null;
                            writer = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
                            writer.write("getPro;" + proId);
                            writer.write("ends\n");
                            writer.flush();

                            DataInputStream inputStream = null;
                            try {
                                inputStream = getMessageStream();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            String basePath = "D:\\javafx\\program\\";
                            int bufferSize = 8192;
                            byte[] buf = new byte[bufferSize];
                            int passedlen = 0;
                            long len = 0;
                            String fileName = inputStream.readUTF();
                            final String savePath = basePath + fileName;
//                                List<InputStream> list=new ArrayList<InputStream>();
//                                list.add(new FileInputStream(new File(savePath)));
//                                list.add(inputStream);
//                                Enumeration<InputStream> eum=Collections.enumeration(list);
//                                SequenceInputStream sis=new SequenceInputStream(eum);

//                                System.out.println("文件名:"+sis.available());

                            System.out.println("文件名:" + savePath);
                            DataOutputStream fileOut = new DataOutputStream(new BufferedOutputStream(new BufferedOutputStream(new FileOutputStream(savePath, true))));
                            System.out.println("文件名:" + savePath);
                            len = inputStream.readLong();

                            System.out.println("文件的长度为:" + len + "\n");
                            System.out.println("开始接收文件!" + "\n");
                            while (true) {
                                int read = 0;
                                if (inputStream != null) {
                                    read = inputStream.read(buf);
                                }
                                passedlen += read;
                                if (read == -1) {
                                    break;
                                }
                                CacheManager.putContent("jindu", fileName, String.valueOf(((passedlen + Long.valueOf(oldLength)) * 100 / (len))), String.valueOf(len), String.valueOf(passedlen + Long.valueOf(oldLength)));
                                fileOut.write(buf, 0, read);
                            }
                            System.out.println("接收完成，文件存为" + savePath + "\n");
                            System.out.println("接收完成，文件存为" + CacheManager.getContent("jindu").getValue());
                            writer.close();
                            inputStream.close();
                            fileOut.close();

                            final String path = basePath + proId;

                            Thread th = new Thread() {
                                public void run() {
                                    try {
                                        ZipUtils.unzipFile(savePath, path);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    File f = new File(savePath);
                                    if (f.exists()) {
                                        f.delete();
                                    }
                                    try {
                                        Thread.sleep(1500);
                                    } catch (Exception e) {
                                    }
                                }
                            };
                            th.start();

//                                FileToZip.showZip(savePath, path);
//                                File f=new File(savePath);
//                                if(f.exists()){
//                                    f.delete();
//                                }

                        }
                    }
                    socket.close();

                }


//                    Thread th=new Thread(){
//                        public void run(){
//
//
//                            Platform.runLater(() -> {
//                                Main main = new Main();
//                                Timer timer = new Timer();
//                                main.updateStage1(timer);
//                            });

//                    Platform.runLater(() -> {
//                                    Thread th=new Thread(){
//                                        public void run(){
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    public void run() {
                        String planPath = "D:\\javafx\\program";
                        int count = FileUtils.getZipFileNames(planPath);
                        if (count == 0) {
                            if (TextUtils.isEmpty(planStr)) {
                                String planPath1 = "D:\\javafx\\plan\\plan.xml";
                                Map m = new HashMap();
                                m.put("planContext", planStr);
                                File planFile = new File(planPath1);
                                if (planFile.exists())
                                    planFile.delete();
                                FileUtils.writeFileFromString(planPath, planStr, false);
//                                XmlUtil.createXml(planPath1, "plan", "text", m);
                            }
//                            Main main = new Main();
//                            main.restartMe();
                        }


                    }
                }, 3000, 5000);
//                        Main main = new Main();
//                        main.restartMe();
//                                            try {
//                                                Thread.sleep(500);
//                                            } catch (Exception e) {}
//                                        }
//                                    };
//                                    th.start();

//                                    Timer timer = new Timer();

//                                    main.updateStage1(timer);

//                    });
//                            try {
//                                Thread.sleep(500);
//                            } catch (Exception e) {}
//                        }
//                    };
//                    th.start();
            }
            if (sb.toString().split(";")[0].equals("10")) {
                String programId = sb.toString().split(";")[1];
                String basePath = "D:\\javafx\\program\\";
                File file = new File(basePath + programId + ".zip");
                String fileCount = "no";
                if (file.exists()) {
                    fileCount = String.valueOf(file.length());
                }
                System.out.print(fileCount + "++++++++++++size");
                Writer writer = null;
                writer = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
                writer.write(fileCount);
                writer.write("ends\n");
                writer.flush();
                writer.close();
                socket.close();
            }
        } catch (IOException ex) {
            Log.d("show123", "run() called");
        } finally {

        }
    }

    public static byte[] File2byte(String filePath) {
        byte[] buffer = null;
        try {
            File file = new File(filePath);
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            fis.close();
            bos.close();
            buffer = bos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer;
    }

    /**
     * 把Bitmap转Byte
     */
    public static byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    DataOutputStream out = null;
    DataInputStream getMessageStream = null;

    public DataInputStream getMessageStream() throws Exception {
        try {
            getMessageStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
            return getMessageStream;
        } catch (Exception e) {
            e.printStackTrace();
            if (getMessageStream != null)
                getMessageStream.close();
            throw e;
        } finally {
        }
    }
}
